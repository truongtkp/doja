package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.domain.Address;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.GoogleMapServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/map")
@RequiredArgsConstructor
@Tag(name = "GoogleMap",description = "OK")
public class GoogleMapResource {
    private final Logger log = LoggerFactory.getLogger(GoogleMapResource.class);
    private final GoogleMapServiceImpl googleMapService;

    @GetMapping("search")
    public ResponseEntity<Map<String,?>> searchByQuery(@RequestParam("q") String query) {
        List<Address> addresses = googleMapService.getAddressList(query);
        HashMap<String,?> res = ArrayDTO.convertListToSet(addresses.size(),addresses);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }
}
