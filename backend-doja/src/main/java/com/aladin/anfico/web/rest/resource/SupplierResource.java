package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Social;
import com.aladin.anfico.domain.SupplierInfo;
import com.aladin.anfico.repository.SupplierInfoRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.supplier.SupplierInfoDTO;
import com.aladin.anfico.service.impl.SupplierInfoServiceImpl;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "SupplierInfo",description = "OK")
public class SupplierResource {

    private final Logger log = LoggerFactory.getLogger(SupplierResource.class);

    private final SupplierInfoRepository supplierInfoRepository;
    private final SupplierInfoServiceImpl supplierInfoServiceImpl;

    @GetMapping("/suppliers")
    public ResponseEntity<HashMap<String,?>> getAllSupplierInfo(@RequestParam int page, @RequestParam int limit ) {
        log.info("--GET: /api/history -- Get all information history table--");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> history =  supplierInfoServiceImpl.getAllManagedSupplierInfo(page,limit);
        return new ResponseEntity<>(history, HttpStatus.OK);
    }

    @PostMapping("/supplier")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<SupplierInfoDTO> createSupplierInfo(@Valid @RequestBody SupplierInfoDTO supplierInfo ) {
        log.info("--POST: /api/supplier -- Add information supplier table--");
        if (supplierInfo.getId() != null) {
            throw new BadRequestAlertException(Alerts.ID_NULL, "suppler-management", "id");
        }
        return new ResponseEntity<>(supplierInfoServiceImpl.addSupplier(supplierInfo), HttpStatus.OK);
    }

    @PutMapping("/supplier")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<SupplierInfo> updateSupplierInfo(@Valid @RequestBody SupplierInfoDTO supplierInfo ){
        log.info("--PUT: /api/supplier -- Update information supplier table--");
        return ResponseUtil.wrapOrNotFound(supplierInfoServiceImpl.updateSupplierInfo(supplierInfo));
    }

    @DeleteMapping("/supplier/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteSupplierInfo(@PathVariable Long id ){
        log.info("--DELETE: /api/history -- delete information history table--");
        if ((!supplierInfoServiceImpl.deleteSupplierInfo(id).isPresent())){
            throw new BadRequestAlertException(Alerts.ID_EXIST, "suppler-management", "id");
        }
        return new ResponseEntity<>(true,HttpStatus.OK);
    }

    @GetMapping("/supplier/{id}")
    public ResponseEntity<SupplierInfo> getDetailSupplierInfo(@PathVariable Long id) {
        log.info("--GET: /api/supplier -- Get  information history table--");
        return ResponseUtil.wrapOrNotFound(supplierInfoRepository.findById(id));
    }

    @GetMapping("/supplier/type/{social}")
    public ResponseEntity<List<SupplierInfo>> getSupplierInfoByType(@PathVariable Social social) {
        log.info("--GET: /api/supplier -- Get  information history table--");
        return ResponseEntity.ok().body(supplierInfoRepository.getSupplierInfoBySocial(social));
    }
}
