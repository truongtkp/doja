package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.*;
import com.aladin.anfico.repository.*;
import com.aladin.anfico.service.PaypalService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.MailService;
import com.aladin.anfico.service.impl.PaypalServiceImpl;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Booking",description = "booking")
public class BookingResource {
    private final Logger log = LoggerFactory.getLogger(BookingResource.class);
    private final AddressRepository addressRepository;
    private final PackageRepository packageRepository;
    private final StaffRepository staffRepository;
    private final BookingRepository bookingRepository;
    private final TimeTableRepository timeTableRepository;
    private final VoucherRepository voucherRepository;

    private final PaypalServiceImpl paypalServiceImpl;
    private final ObjectMapper objectMapper;

    private final Validator validator;

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private final TypeReference<Map<String,Object>> tr = new TypeReference<>() {};
    private final ObjectMapper om;

    private final MailService mailService;


    @PostMapping("/booking")
    public ResponseEntity<Booking> addBooking(
//        @Valid @RequestBody BookingCreateDTO bookingCreateDTO
        @RequestBody HashMap<String,Object> bookingReq
    ) throws IOException, URISyntaxException {
        log.info("--POST: /api/booking Add booking --");

        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);

        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer) bookingReq.get("addressId")));
        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");

        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");

        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");

        Date bookingDate;
        try {
            bookingDate = df.parse((String) bookingReq.get("bookingDate"));
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }

//        Optional<TimeTable> timeTable = timeTableRepository.findById(new Time(
//            (Integer) bookingReq.get("hour"),
//            (Integer) bookingReq.get("minute"),
//            0
//        ));
//        if (timeTable.isEmpty())throw new BadRequestAlertException("Timetable not found","","");


        booking.setOrderId(null);
        booking.setAddress(address.get());
        booking.setServicePackage(servicePackage.get());
        booking.setStaff(staff.get());
        booking.setBookingDate(bookingDate);
        booking.setTimetable(
            new Time(
            (Integer) bookingReq.get("hour"),
            (Integer) bookingReq.get("minute"),
            0
            )
//            timeTable.get()
        );


        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");
        temp = om.readValue(paypalServiceImpl.createOrderBookingV2(bearerToken,servicePackage.get().getPrice())
            .getResponseBody()
            ,tr);
        log.info(temp.toString());
        booking.setOrderId((String) temp.get("id"));
        Booking res = bookingRepository.save(booking);

        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PostMapping("/booking/confirm/{id}")
    public ResponseEntity<Map<String,Object>> confirmBooking(@PathVariable String id) throws IOException, URISyntaxException {
        if (bookingRepository.findBookingByPaypalOrderId(id).isEmpty())
            throw new BadRequestAlertException("Paypal order id not found","","");
        Map<String,Object> temp = om.readValue(paypalServiceImpl.generateBearerAccessToken().getResponseBody(),tr);
        String bearerToken = (String) temp.get("access_token");
        temp = om.readValue(paypalServiceImpl.capturePaymentV2(bearerToken,id).getResponseBody(),tr);
        return new ResponseEntity<>(temp,HttpStatus.OK);
    }

    @GetMapping("/booking")
    public ResponseEntity<HashMap<String,?>> getAllBooking(){
        List<Booking> bookings = bookingRepository.findAll();
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PutMapping("/booking")
    public ResponseEntity<Booking> updateBooking(
        @RequestBody HashMap<String, Object> bookingReq
    ){
        Booking booking = objectMapper.convertValue(bookingReq,Booking.class);
        Set<ConstraintViolation<Booking>> violations = validator.validate(booking);
        if (!violations.isEmpty() || booking.getId() == null)
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        Optional<Address> address = addressRepository.findById(Long.valueOf((Integer)bookingReq.get("addressId")));
        if (address.isEmpty())throw new BadRequestAlertException("Address not found","","");

        Optional<Packages> servicePackage = packageRepository.findById(Long.valueOf((Integer) bookingReq.get("packageId")));
        if (servicePackage.isEmpty())throw new BadRequestAlertException("Service (Package) not found","","");

        Optional<Staff> staff = staffRepository.findById(Long.valueOf((Integer) bookingReq.get("staffId")));
        if (staff.isEmpty())throw new BadRequestAlertException("Staff not found","","");
        if (!violations.isEmpty())
            throw new BadRequestAlertException("Validation failed",violations.toString(),"");

        booking.setAddress(address.get());
        booking.setServicePackage(servicePackage.get());
        booking.setStaff(staff.get());

        Optional<Booking> res = Optional
            .of(bookingRepository.findById(booking.getId()))
            .filter(Optional::isPresent)
            .map(param -> bookingRepository.save(booking));

        return ResponseUtil.wrapOrNotFound(res);
    }

    @GetMapping ("/booking/{id}")
    public ResponseEntity<Booking> getBookingById(@PathVariable Long id) {
        Optional<Booking> booking = bookingRepository.findById(id);
        if (booking.isEmpty()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
        }
        return ResponseEntity.ok().body(booking.get());
    }

    @DeleteMapping("/booking/{id}")
    public ResponseEntity<Boolean> deleteBooking(@PathVariable Long id) {
        Optional
            .of(bookingRepository.findById(id))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                bookingRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {}, () -> {
                throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "", "id");
            });

        return ResponseEntity.ok().body(true);
    }


    @GetMapping("/booking/search/date")
    public ResponseEntity<HashMap<String, ?>> searchBookingByDateRange(
        @RequestParam String from, @RequestParam String to
    ) {
        log.info("--GET: /api/address -- Get product detail in product table--");

        Date fromDate;
        Date toDate;
        try {
            fromDate = df.parse(from);
            toDate = df.parse(to);
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }
        List<Booking> bookings = bookingRepository.searchBookingByDateRange(fromDate,toDate);
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/booking/search/date/{at}")
    public ResponseEntity<HashMap<String, ?>> searchBookingByDate(@PathVariable String at
    ) {
        log.info("--GET: /api/date -- Get product detail in product table--");

        Date atDate;
        try {
            atDate = df.parse(at);
        } catch (ParseException e) {
            throw new BadRequestAlertException("Invalid date format","","");
        }
        log.info("Date validation passed! => " + atDate.toString());

        List<Booking> bookings = bookingRepository.searchBookingByDate(at+" 00:00:00", at+ " 23:59:59");
        HashMap<String,?> res = ArrayDTO.convertListToSet(bookings.size(),bookings);
        return ResponseEntity.ok().body(res);
    }
}
