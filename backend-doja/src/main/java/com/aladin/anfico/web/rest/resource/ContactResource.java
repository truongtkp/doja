package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Contact;
import com.aladin.anfico.repository.ContactRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.ContactService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.ContactServiceImpl;
import com.aladin.anfico.service.dto.contact.ContactCreateDTO;
import com.aladin.anfico.service.dto.contact.ContactUpdateDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import com.aladin.anfico.web.rest.errors.PhoneFormatException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "Contact",description = "OK")
public class ContactResource {

    private final Logger log = LoggerFactory.getLogger(ContactResource.class);
    private final ContactService contactServiceImpl;



    @PostMapping("/contact")
    public ResponseEntity<?> createContact(@Valid @RequestBody ContactCreateDTO contactCreateDTO){
        log.info("[Anfico App]: POST /api/contact -- Create a contact on table Contact");
        if (contactCreateDTO.getId() != null) {
            throw new BadRequestAlertException(Alerts.ID_NULL, "contact-management", "id");
        }
        return ResponseEntity.ok().body(contactServiceImpl.addContact(contactCreateDTO));
    }

    @GetMapping("/contacts")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> getAllContact(@RequestParam int page, @RequestParam int size){
        log.info("[Anfico App]: GET /api/contacts -- Get all  contact on table Contact");
        if (!ArrayDTO.checkPageable(page,size)) throw new PageableRequestAlertException();
        return  ResponseEntity.ok().body( contactServiceImpl.getAllContact(page, size));
    }

    @GetMapping("/contact/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Contact> getContact(@PathVariable Long id){
        log.info("[Anfico App]: GET /api/contact -- Get a contact on table Contact");
        return ResponseUtil.wrapOrNotFound(contactServiceImpl.getContactById(id));
    }

    @GetMapping("/contact/search/{param}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> searchContact(@PathVariable String param, @RequestParam int page, @RequestParam int limit){
        log.info("[Anfico App]: GET /api/contact -- Get a contact on table Contact");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return ResponseEntity.ok().body(contactServiceImpl.searchContactByAll(param, page, limit));
    }

    @PutMapping("/contact")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Contact> isStatus(@RequestBody ContactUpdateDTO contactUpdateDTO){
        log.info("[Anfico App]: PUT /api/contact -- Send feedback contact on table Contact");
        return ResponseUtil.wrapOrNotFound(contactServiceImpl.updateContact(contactUpdateDTO));
    }

    @DeleteMapping("/contact/{id}")
    public ResponseEntity<Boolean> deleteContact(@PathVariable Long id){
        log.info("DELETE update status");
        Optional<Contact> contact =  contactServiceImpl.deleteContactById(id);
        if (!contact.isPresent()) {
           return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(true);
    }

}
