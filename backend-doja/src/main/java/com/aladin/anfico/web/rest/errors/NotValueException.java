package com.aladin.anfico.web.rest.errors;

public class NotValueException extends BadRequestAlertException{

    public NotValueException(String txt) {
        super(ErrorConstants.NOT_VALUE_READY, txt , "", "no value");
    }
}
