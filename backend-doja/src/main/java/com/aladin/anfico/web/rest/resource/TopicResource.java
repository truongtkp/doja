package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.domain.TopicImage;
import com.aladin.anfico.domain.Type;
import com.aladin.anfico.repository.TopicRepository;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.impl.TopicServiceImpl;
import com.aladin.anfico.service.dto.topic.DeviceDTO;
import com.aladin.anfico.service.dto.topic.TopicDTO;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class TopicResource {

    private final TopicServiceImpl topicServiceImpl;
    private final TopicRepository topicRepository;
    private final Logger log = LoggerFactory.getLogger(TopicServiceImpl.class);


//    Banner
    @GetMapping("/banner")
    public ResponseEntity<Set<TopicImage>> getBanner(){
        log.info("[GET] TopicResource: /api/banner");
        return new ResponseEntity<>(topicServiceImpl.getTopicBanner(Type.BANNER).get(), HttpStatus.OK);
    }
    @PostMapping("/banner")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> addBanner(@RequestBody List<TopicImage> topicImages){
        log.info("[Anfico App]: POST /api/banner -- upload image in banner");
        return new ResponseEntity<>(topicServiceImpl.addTopicImage(topicImages, Type.BANNER), HttpStatus.OK);
    }
    @DeleteMapping("/banner/image/{id}")
    public ResponseEntity<Boolean> deleteImageBanner(@PathVariable Long id){
        log.info("[Anfico App]: DELETE /banner/image/{id} -- delete image in banner");
        topicServiceImpl.deleteTopicImage(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }


//    Panner
    @GetMapping("/partner")
    public ResponseEntity<Set<TopicImage>> getPartner(){
        log.info("[Anfico App]: GET /api/partner -- get image in panner");
        return new ResponseEntity<>(topicServiceImpl.getTopicBanner(Type.PARTNER).get(), HttpStatus.OK);
    }

    @PostMapping("/partner")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> addPartner(@RequestBody List<TopicImage> topicImages){
        log.info("[Anfico App]: POST /api/partner -- upload image in partner");
        return new ResponseEntity<>(topicServiceImpl.addTopicImage(topicImages, Type.PARTNER), HttpStatus.OK);
    }
    @DeleteMapping("/partner/image/{id}")
    public ResponseEntity<Boolean> deleteImagePartner(@PathVariable Long id){
        log.info("[Anfico App]: DELETE /api/partner/image/{id} -- delete image in partner");
        topicServiceImpl.deleteTopicImage(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }


//    Album
    @GetMapping("/album")
    public ResponseEntity<List<DeviceDTO>> getAlbum(){
        log.info("[Anfico App]: GET /api/album -- get all album");
        return new ResponseEntity<>(topicServiceImpl.getAllTopic(Type.ALBUM).get(), HttpStatus.OK);
    }
    @GetMapping("/album/{id}")
    public ResponseEntity<Topic> getAlbum(@PathVariable Long id){
        log.info("[Anfico App]: GET /api/album/{id} -- get image in album");
        return new ResponseEntity<>(topicRepository.findById(id).get(), HttpStatus.OK);
    }
    @DeleteMapping("/album/{id}")
    public ResponseEntity<Boolean> deleteAlbum(@PathVariable Long id){
        log.info("[Anfico App]: DELETE /api/album/{id} -- delete a album");
        if (topicServiceImpl.deleteAlbum(id)) return new ResponseEntity<>(true, HttpStatus.OK);
        return new ResponseEntity<>(false , HttpStatus.NOT_FOUND);
    }
    @GetMapping("/album/image/{id}")
    public ResponseEntity<Optional<Set<TopicImage>>> getAlbumId(@PathVariable Long id){
        log.info("[Anfico App]: GET /api/album/image/{id} -- get image in album");
        return new ResponseEntity<>(topicServiceImpl.getTopicAlbum(id), HttpStatus.OK);
    }

    @PostMapping("/album")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Topic> addAlbum(@RequestBody TopicDTO topicDTO){
        log.info("[Anfico App]: POST /api/albumn -- add a album");
        return new ResponseEntity<>(topicServiceImpl.addTopic(Type.ALBUM, topicDTO), HttpStatus.OK);
    }
    @PostMapping("/album/image/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> addImageAlbum(@RequestBody List<TopicImage> topicImages, @PathVariable Long id){
        log.info("[Anfico App]: POST /api/albumn -- upload list image in album");
        return new ResponseEntity<>(topicServiceImpl.addListAlbum(topicImages, Type.ALBUM, id), HttpStatus.OK);
    }

    @PutMapping("/album")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Topic> updatePartner(@RequestBody TopicDTO topicDTO){
        log.info("[Anfico App]: PUT /api/album -- update image in album");
        return new ResponseEntity<>(topicServiceImpl.updateTopic(topicDTO), HttpStatus.OK);
    }

    @DeleteMapping("/album/image")
    public ResponseEntity<Boolean> deleteImageAlbum(@RequestBody List<Long> id){
        log.info("[Anfico App]: DELETE /api/album/image -- delete list image in album");
        topicServiceImpl.deleteTopicImage(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

//devices

    @GetMapping("/devices")
    public ResponseEntity<HashMap<String, ?>> getDevice(@RequestParam int page, @RequestParam int limit){
        log.info("[Anfico App]: GET /api/devices -- get all devices");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return new ResponseEntity<>(topicServiceImpl.getAllTopic(page, limit), HttpStatus.OK);
    }
    @PostMapping("/device")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DeviceDTO> addDevice(@RequestBody DeviceDTO topicDTO){
        log.info("[Anfico App]: POST /api/device -- add a  device");
        return new ResponseEntity<>(topicServiceImpl.addDevice(topicDTO), HttpStatus.OK);
    }
    @PutMapping("/device")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<DeviceDTO> updateDevice(@RequestBody DeviceDTO topicDTO){
        log.info("[Anfico App]: PUT /api/banner -- update a device");
        return ResponseUtil.wrapOrNotFound(topicServiceImpl.updateDevice(topicDTO));
    }

    @DeleteMapping("/device/{id}")
    public ResponseEntity<Boolean> deleteDevice(@PathVariable Long id){
        log.info("[Anfico App]: POST /api/device/{id} -- delete device ");
        if (topicServiceImpl.deleteDevice(id))   return new ResponseEntity<>(true, HttpStatus.OK);
        return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/device/search/{param}")
    public ResponseEntity<HashMap<String, ?>> getDevice(@PathVariable String param, @RequestParam int page, @RequestParam int limit){
        log.info("[Anfico App]: Get /api/device/search/{param} -- search device");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return new ResponseEntity<>(topicServiceImpl.searchDevice(param, page, limit), HttpStatus.OK);
    }


}
