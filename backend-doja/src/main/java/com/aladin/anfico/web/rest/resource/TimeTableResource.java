package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.TimeTable;
import com.aladin.anfico.repository.TimeTableRepository;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "TimeTable",description = "timetable")
public class TimeTableResource {
    private final TimeTableRepository timeTableRepository;

    @GetMapping("/timetable")
    public ResponseEntity<HashMap<String,?>> getTimeTable(@RequestBody(required = false)HashMap<String,Object> reqBody) {
        class ReqBodyParam {
            String sort;
        }

        List<TimeTable> times;
        CHECK_BODY:
        {
            if (reqBody == null) {
                times = timeTableRepository.findAll();
                break CHECK_BODY;
            }

            ReqBodyParam reqBodyParam = new ReqBodyParam();
            reqBodyParam.sort = (String) reqBody.get("sort");

            if (Objects.equals(reqBodyParam.sort, "asc")){
                List<Sort.Order> sortOrders = new ArrayList<Sort.Order>() {{
                    add(new Sort.Order(Sort.Direction.ASC,"hour"));
                    add(new Sort.Order(Sort.Direction.ASC,"minute"));
                }};

                times = timeTableRepository.findAll(Sort.by(sortOrders));


                break CHECK_BODY;
            }
            if (Objects.equals(reqBodyParam.sort, "desc")){
                List<Sort.Order> sortOrders = new ArrayList<Sort.Order>() {{
                    add(new Sort.Order(Sort.Direction.DESC,"hour"));
                    add(new Sort.Order(Sort.Direction.DESC,"minute"));
                }};

                times = timeTableRepository.findAll(Sort.by(sortOrders));
                break CHECK_BODY;
            }

            // USE DEFAULT GET
            times = timeTableRepository.findAll();
        }

        HashMap<String,?> res = ArrayDTO.convertListToSet(times.size(),times);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/timetable")
    public ResponseEntity<TimeTable> addTimetable(@RequestBody HashMap<String,Object> timetableReq){
        Time time = new Time(
            (Integer) timetableReq.get("hour"),
            (Integer) timetableReq.get("minute"),
            0
        );

        if (timeTableRepository.findById(time).isPresent()){
            throw new BadRequestAlertException(Alerts.ID_EXIST, "", "id");
        }
        TimeTable res = timeTableRepository.save(new TimeTable(time));
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @DeleteMapping("/timetable")
    public ResponseEntity<Boolean> deleteTimetable(@RequestBody HashMap<String,Object> timetableReq){
        Time time = new Time(
            (Integer) timetableReq.get("hour"),
            (Integer) timetableReq.get("minute"),
            0
        );

        Optional
            .of(timeTableRepository.findById(time))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(param -> {
                timeTableRepository.delete(param);
                return param;
            }).ifPresentOrElse( (param) -> {},
                () -> {
                    throw new BadRequestAlertException(Alerts.ID_EXIST, "", "id");
                }
            );

        return ResponseEntity.ok(true);
    }
}
