package com.aladin.anfico.domain;


import com.aladin.anfico.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@Table(name = "history")
@AllArgsConstructor
@NoArgsConstructor
public class History extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 4, unique = true)
    @Pattern(regexp = Constants.YEAR_REGEX)
    private String year;

    @Column(name = "image_path", length = 500)
    @Size(min = 0, max = 500)
    private String imagePath;

    @Column(name = "image_url", length = 500)
    @Size(min = 0, max = 500)
    private String imageUrl;

    @Column(name = "description_vi", columnDefinition = "LONGTEXT")
    private String descriptionVi;

    @Column(name = "description_en", columnDefinition = "LONGTEXT")
    private String descriptionEn;


}
