package com.aladin.anfico.domain;

import com.aladin.anfico.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Data
@Table(name = "staff")
@AllArgsConstructor
@NoArgsConstructor
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name="name")
    private String name;

    @Column
    private String image;

    @Column
    private Date birthday;

    @Email
    @Column(name="email")
    private String email;

    @Pattern(regexp = Constants.PHONE_REGEX)
    @NotNull
    @Column(name="phone_no")
    private String phoneNo;

    @Column
    private String description;

    @NotNull
    @Column
    private String address;
}
