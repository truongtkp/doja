package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "order_status")
@AllArgsConstructor
@NoArgsConstructor
public class OrderStatus {
    @Id
    private String codeId;
}
