package com.aladin.anfico.domain;
import com.aladin.anfico.config.Constants;
import lombok.*;
import org.aspectj.apache.bcel.classfile.Constant;

import javax.annotation.RegEx;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "contact")
@AllArgsConstructor
@NoArgsConstructor
public class Contact extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 256)
    @Size(min = 5, max = 256)
    private String fullname;

    @Column(length = 256)
    @Size(min = 5, max = 256)
    private String companyName;

    @Column(length = 256)
    @Size(min = 5, max = 256)
    private String companyAddress;

    @Email
    @Size(min = 5, max = 256)
    @Column(length = 256)
    private String email;

    @Column(length = 10)
    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;


    @Column(length = 100)
    @Size(min = 0, max = 100)
    private String fromTo;

    @Column(columnDefinition = "LONGTEXT")
    private String note;

    @Column(columnDefinition = "LONGTEXT")
    private String feedback;

    @NotNull
    @Column(nullable = false)
    private boolean status;
}
