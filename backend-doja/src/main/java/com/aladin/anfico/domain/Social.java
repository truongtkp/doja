package com.aladin.anfico.domain;

public enum Social {
    PHONE, GMAIL, SKYPE, ZALO, FACEBOOK
}
