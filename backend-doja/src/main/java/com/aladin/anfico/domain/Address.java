package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "address")
@AllArgsConstructor
@NoArgsConstructor
public class Address {
//    @NotNull
//    @Id
//    private String id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column
    private String name;

    @Column(name = "description", columnDefinition = "LONGTEXT")
    private String description;

    @NotNull
    @Column
    private String address;

//    @Column
//    private Double latitude;
//
//    @Column
//    private Double longitude;
}
