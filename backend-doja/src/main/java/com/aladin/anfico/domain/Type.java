package com.aladin.anfico.domain;

public enum Type {
    BANNER, PARTNER, DEVICE, ALBUM
}
