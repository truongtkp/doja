package com.aladin.anfico.domain;

import java.io.Serializable;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "recruit")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Recruit extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public Recruit(Long id, String titleVi) {
        this.id = id;
        this.titleVi = titleVi;
        this.candidates = null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en", length = 500)
    @Size(max = 500)
    private String titleEn;

    @Column(name = "title_vi", length = 500)
    @Size(max = 500)
    private String titleVi;

    @Column(name = "workplace_vi", length = 500)
    @Size(max = 500)
    private String workplaceVi;

    @Column(name = "workplace_en", length = 500)
    @Size(max = 500)
    private String workplaceEn;

    @Column(name = "avatar_url", length = 500)
    @Size(max = 500)
    private String avatarUrl;

    @Column(name = "avatar_path", length = 500)
    @Size(max = 500)
    private String avatarPath;

    @Column(name = "date_expiration")
    private Date dateExpiration;

    @Column(name = "describe_vn", columnDefinition = "TEXT")
    private String describeVi;

    @Column(name = "interest_vn", columnDefinition = "TEXT")
    private String interestVi;

    @Column(name = "require_vn", columnDefinition = "TEXT")
    private String requireVi;

    @Column(name = "describe_en", columnDefinition = "TEXT")
    private String describeEn;

    @Column(name = "interest_en", columnDefinition = "TEXT")
    private String interestEn;

    @Column(name = "require_en", columnDefinition = "TEXT")
    private String requireEn;

    @Column(name = "salary_vi", length = 256)
    @Size(max = 256)
    private String salaryVi;

    @Column(name = "salary_en", length = 256)
    @Size(max = 256)
    private String salaryEn;

    @Column(name = "num_person")
    private int numPerson;


    @Column(name = "level_vi", length = 256)
    @Size(max = 256)
    private String levelVi;

    @Column(name = "level_en", length = 256)
    @Size(max = 256)
    private String levelEn;

    @Column(name = "gender_vi", length = 40)
    @Size(max = 40)
    private String genderVi;

    @Column(name = "gender_en", length = 40)
    @Size(max = 40)
    private String genderEn;


    @Column(name = "experience_vi", length = 100)
    @Size(max = 100)
    private String experienceVi;

    @Column(name = "experience_en", length = 100)
    @Size(max = 100)
    private String experienceEn;

    @Column(name = "workingForm_vi", length = 100)
    @Size(max = 100)
    private String  workingFormVi;

    @Column(name = "workingForm_en", length = 100)
    @Size(max = 100)
    private String  workingFormEn;

    @OneToMany(mappedBy = "recruit", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Candidate> candidates;

    public Recruit(Long id, String titleEn, String titleVi, String avatarUrl, String describe_vn, String describe_en) {
        this.id = id;
        this.titleEn = titleEn;
        this.titleVi = titleVi;
        this.avatarUrl = avatarUrl;
        this.describeEn = describe_vn;
        this.describeVi = describe_en;
    }
}
