package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Data
@Table(name = "order_voucher")
@AllArgsConstructor
@NoArgsConstructor
public class OrderVoucher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Voucher voucher;

//    @OneToOne(cascade = CascadeType.MERGE)
//    private OrderStatus orderStatus;
    @Column(name = "order_status")
    private Boolean orderStatus;

    @Column(name="order_id")
    private String orderId;

    @NotNull
    @Column(name="`to`",length = 256)
    @Size(min = 5, max = 256)
    private String to;
    @NotNull
    @Column(name="`from`",length = 256)
    @Size(min = 5, max = 256)
    private String from;

    @Email
    @Column(name="email")
    private String email;

    @NotNull
    @Column(name="phone_no")
    private String phoneNo;

    @NotNull
    @Column(name="expiration_date")
    private Date expirationDate;
}
