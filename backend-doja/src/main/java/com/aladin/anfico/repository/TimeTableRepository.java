package com.aladin.anfico.repository;

import com.aladin.anfico.domain.TimeTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Time;

public interface TimeTableRepository extends JpaRepository<TimeTable, Time> {
}
