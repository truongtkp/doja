package com.aladin.anfico.repository;

import com.aladin.anfico.domain.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderStatusRepository extends JpaRepository<OrderStatus,String> {
}
