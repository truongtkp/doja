package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Contact;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.RecruitFillterDTO;
import com.aladin.anfico.service.dto.search.SearchDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecruitRepository extends JpaRepository<Recruit,Long> {

    @Query("select new Recruit(r.id,r.titleVi) from Recruit r order by r.id desc")
    List<Recruit> getAllRecruit();

    @Query("select c from Recruit c where c.titleEn like %?1% ")
    List<Recruit> searchRecruitsByAllEn(String param, PageRequest pageRequest);

    @Query("select count(c.id) from Recruit c where c.titleEn like %?1% ")
    int countSearchEn(String param);

    @Query("select c from Recruit c where  c.titleVi like %?1% ")
    List<Recruit> searchRecruitsByAllVi(String param, PageRequest pageRequest);

    @Query("select count(c.id) from Recruit c where  c.titleVi like %?1% ")
    int countSearchVi(String param);

    @Query("select new Recruit(c.id, c.titleEn, c.titleVi, c.avatarUrl, c.describeVi, c.describeEn) from Recruit c where c.titleEn like %?1% ")
    List<SearchDTO> searchAllByRecruitEn(String param);

    @Query("select new Recruit(c.id, c.titleEn, c.titleVi, c.avatarUrl, c.describeVi, c.describeEn)  from Recruit c where  c.titleVi like %?1% ")
    List<SearchDTO> searchAllByRecruitVi(String param);
}
