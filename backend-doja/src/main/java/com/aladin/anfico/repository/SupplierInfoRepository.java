package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Social;
import com.aladin.anfico.domain.SupplierInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface SupplierInfoRepository extends JpaRepository<SupplierInfo, Long> {

    List<SupplierInfo> getSupplierInfoBySocial(Social social);

}
