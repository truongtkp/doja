package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecificationRepository extends JpaRepository<Specification, Long> {
}
