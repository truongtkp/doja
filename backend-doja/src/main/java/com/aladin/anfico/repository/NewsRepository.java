package com.aladin.anfico.repository;

import com.aladin.anfico.domain.News;
import com.aladin.anfico.service.dto.search.SearchDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long> {
    @Query("select c from News c where c.titleVi like %?1%  ")
    List<News> searchNewsByAll(String param, PageRequest pageRequest);
    @Query("select count(c.id) from News c where c.titleVi like %?1%  ")
    int countSearch(String param);


    @Query("select new News(c.id, c.titleVi, c.titleEn, c.descriptionVi, c.descriptionEn, c.avatarUrl) from News c where c.titleEn like %?1%  ")
    List<SearchDTO> searchAllByNewsEn(String param);

    @Query("select new News(c.id, c.titleVi, c.titleEn, c.descriptionVi, c.descriptionEn, c.avatarUrl) from News c where c.titleVi like %?1%  ")
    List<SearchDTO> searchAllByNewsVi(String param);

}
