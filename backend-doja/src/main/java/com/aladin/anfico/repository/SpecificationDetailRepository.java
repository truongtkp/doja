package com.aladin.anfico.repository;

import com.aladin.anfico.domain.SpecificationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecificationDetailRepository extends JpaRepository<SpecificationDetail,Long> {
}
