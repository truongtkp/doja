package com.aladin.anfico.repository;

import com.aladin.anfico.domain.OrderVoucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderVoucherRepository extends JpaRepository<OrderVoucher,Long> {
    @Query("select o.id from OrderVoucher o where o.voucher.id = ?1 ")
    List<Long> findOrderIdByVoucherId(Long voucherId);

//    @Query("select new OrderVoucher(o.id,o.voucher,o.orderStatus,o.balance,o.orderId,o.to,o.from,o.email,o.phoneNo,o.expirationDate) " +
//        "from OrderVoucher o where o.voucher.id = ?1 ")
//    @Query("select new OrderVoucher(o.id) "+
    @Query(value = "SELECT * FROM order_voucher WHERE voucher_id = ?1", nativeQuery = true)
    List<OrderVoucher> findOrderByVoucherId(Long voucherId);

    @Query(value = "SELECT * FROM order_voucher WHERE order_id = ?1", nativeQuery = true)
    List<OrderVoucher> findOrderByPaypalOrderId(String orderId);
}
