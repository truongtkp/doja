package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Address;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address,Long> {

    @Query("select new Address(a.id,a.name,a.description,a.address) from " +
        "Address a where a.name like %?1% OR a.address like %?1% group by a.id")
    List<Address> searchAddress(String param, PageRequest pageRequest);

    @Query("select new Address(a.id,a.name,a.description,a.address) from " +
        "Address a where a.name like %?1% OR a.address like %?1% group by a.id")
    List<Address> searchAddress(String param);
}
