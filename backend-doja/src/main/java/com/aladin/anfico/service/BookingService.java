package com.aladin.anfico.service;

import com.aladin.anfico.domain.Booking;
import com.aladin.anfico.service.dto.BookingCreateDTO;

public interface BookingService {
    Booking addBooking(BookingCreateDTO booking);
}
