package com.aladin.anfico.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingCreateDTO {
    private Long id;

    @NotNull
    private Long addressId;

    @NotNull
    private Long servicePackage;

    @NotNull
    private Date bookingDate;

    @NotNull
    private String fullName;

    @NotNull
    private String email;

    @NotNull
    private String phoneNo;
}
