package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Address;
import com.aladin.anfico.repository.AddressRepository;
import com.aladin.anfico.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final Logger log = LoggerFactory.getLogger(AddressServiceImpl.class);
    private final AddressRepository addressRepository;

    @Override
    public List<Address> getAllAddresses() {
        log.info("[Doja App]: get all addresses");
        return addressRepository.findAll();
    }
}
