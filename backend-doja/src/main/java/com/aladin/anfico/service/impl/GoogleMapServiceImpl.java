package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.Address;
import com.aladin.anfico.service.dto.paypal.HttpResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoogleMapServiceImpl {
    private final Logger log = LoggerFactory.getLogger(GoogleMapServiceImpl.class);
    private final String uri = "https://www.google.com/s";
    private final String gsRi = "maps";
    private final String pb = "!2i5!4m9!1m3!1d17247.046325365292!2d105.8394535!3d20.9967898!2m0!3m2!1i556!2i889!4f13.1!7i20!10b1!12m13!1m1!18b1!2m3!5m1!6e2!20e3!10b1!16b1!17m1!3e1!20m2!5e2!6b1!19m4!2m3!1i360!2i120!4i8!20m57!2m2!1i203!2i100!3m2!2i4!5b1!6m6!1m2!1i86!2i86!1m2!1i408!2i240!7m42!1m3!1e1!2b0!3e3!1m3!1e2!2b1!3e2!1m3!1e2!2b0!3e3!1m3!1e8!2b0!3e3!1m3!1e10!2b0!3e3!1m3!1e10!2b1!3e2!1m3!1e9!2b1!3e2!1m3!1e10!2b0!3e3!1m3!1e10!2b1!3e2!1m3!1e10!2b0!3e4!2b1!4b1!9b0!22m2!1sLd0rZKODFpeF-Ab_m7z4CA!7e81!23m2!4b1!10b1!24m79!1m27!13m9!2b1!3b1!4b1!6i1!8b1!9b1!14b1!20b1!25b1!18m16!3b1!4b1!5b1!6b1!9b1!12b1!13b1!14b1!15b1!17b1!20b1!21b1!22b0!25b0!27m1!1b0!2b1!5m6!2b1!3b1!5b1!6b1!7b1!10b1!10m1!8e3!11m1!3e1!14m1!3b1!17b1!20m2!1e3!1e6!24b1!25b1!26b1!29b1!30m1!2b1!36b1!39m3!2m2!2i1!3i1!43b1!52b1!54m1!1b1!55b1!56m2!1b1!3b1!65m5!3m4!1m3!1m2!1i224!2i298!71b1!72m4!1m2!3b1!5b1!4b1!89b1!113b1!26m4!2m3!1i80!2i92!4i8!34m18!2b1!3b1!4b1!6b1!8m6!1b1!3b1!4b1!5b1!6b1!7b1!9b1!12b1!14b1!20b1!23b1!25b1!26b1!37m1!1e81!47m0!49m6!3b1!6m2!1b1!2b1!7m1!1e3!67m2!7b1!10b1!69i641";

    private final ObjectMapper om;

    private HttpResponse processHttpResponse(HttpUriRequest httpUriRequest) throws IOException {
        HttpResponse res = new HttpResponse();
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpUriRequest);
        ){
            HttpEntity resEntity = response.getEntity();
            res.setMeta(httpClient);
            byte [] body = EntityUtils.toByteArray(resEntity);
            byte [] shorter = Arrays.copyOfRange(body,5,body.length);
            res.setResponseBody(shorter);

//            String test = EntityUtils.toString(resEntity);
        }
        return res;
    }

    private HttpResponse getAddressesByQuery(String query) throws URISyntaxException,IOException {
        log.info("POST -> GET ADDRESS BY QUERY");
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader(HttpHeaders.CONTENT_TYPE,"application/json");
        List<NameValuePair> params = new ArrayList<>(){{
            add(new BasicNameValuePair("gs_ri",gsRi));
            add(new BasicNameValuePair("pb",pb));
            add(new BasicNameValuePair("q",query));
        }};
        httpGet.setURI(new URIBuilder(httpGet.getURI()).addParameters(params).build());

        return processHttpResponse(httpGet);
    }

    public List<Address> getAddressList(String query) {
        class ListWrapper {
            private Object current;

            ListWrapper(List<?> source) {
                current = source;
            }

            public ListWrapper next(Integer index){
                this.current = ((List<?>) current).get(index);
                return this;
            }

            public Object getCurrentAsObj(){
                return current;
            }

            public List<?> getCurrentAsList(){
                return (List<?>) current;
            }

            public ListWrapper setCurrent(Object current){
                this.current = current;
                return this;
            }
        }


        List<Address> addresses = new ArrayList<>();
        try {
            HttpResponse res = getAddressesByQuery(query);
            String resStr = new String(res.getResponseBody() );
//            String resStr = new String(res.getResponseBody(), "windows-1258");
            List<?> decode = om.readValue(resStr, List.class);
            ListWrapper data = new ListWrapper(decode);

            List<?> resultList = data.next(0).next(1).getCurrentAsList();
            for (Object element : resultList){
                ListWrapper addressArr = data.setCurrent(element).next(22).next(13);
                if (addressArr.getCurrentAsObj() == null) {
                    continue;
                }
                addressArr.next(0);

                Address address = new Address();
                address.setId((Long) addressArr.getCurrentAsList().get(0));
                address.setAddress(
                    StringEscapeUtils.unescapeHtml4(
                        (String) addressArr.getCurrentAsList().get(1)
                    ));

                addressArr.next(3);
                List<?> latLong = addressArr.getCurrentAsList();
                if (latLong != null){
//                    address.setLatitude((Double) latLong.get(2));
//                    address.setLongitude((Double) latLong.get(3));
                }
                addresses.add(address);
//                address
//                address.setLongitude();


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return addresses;
    }
}
