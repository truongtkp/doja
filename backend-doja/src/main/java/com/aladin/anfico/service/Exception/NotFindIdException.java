package com.aladin.anfico.service.exception;

public class NotFindIdException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public NotFindIdException() {
        super("ID not find");
    }
}
