package com.aladin.anfico.service.dto.product;


import com.aladin.anfico.domain.Category;
import com.aladin.anfico.domain.Product;
import com.aladin.anfico.domain.ProductImage;
import com.aladin.anfico.domain.Specification;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreateDTO {

    private Long id;

    @NotNull
    private Long categoryId;
    @NotNull
    private String descriptionEn;
    @NotNull
    private String descriptionVi;
    @NotNull
    private String contentVi;
    @NotNull
    private String contentEn;
    @NotNull
    @Size(min = 0, max = 500)
    private String titleEn;
    @NotNull
    @Size(min = 0, max = 500)
    private String titleVi;
    @NotNull
    private List<SpecificationDTO> specificationList;
    @NotNull
    private List<ProductImageDTO> productImageList;


}
