package com.aladin.anfico.service.dto.array;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArrayDTO{

    public static HashMap<String, ? > convertListToSet(long count, List<?> list){
        HashMap<String, Object> map = new HashMap<>();
        map.put("total", count);
        map.put("list", list);
        return map;
    }

    public static boolean checkPageable(int page, int size){
        if (page < 0 || size <= 0 ) return false;
        return true;
    }


}
