package com.aladin.anfico.service.impl;

import com.aladin.anfico.domain.Booking;
import com.aladin.anfico.repository.BookingRepository;
import com.aladin.anfico.service.BookingService;
import com.aladin.anfico.service.dto.BookingCreateDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {
    private final Logger log = LoggerFactory.getLogger(BookingServiceImpl.class);
    private final BookingRepository bookingRepository;

    @Override
    public Booking addBooking(BookingCreateDTO booking){
        return null;
//        return bookingRepository.save();
    }
}
