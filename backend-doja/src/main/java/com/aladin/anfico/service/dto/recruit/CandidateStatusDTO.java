package com.aladin.anfico.service.dto.recruit;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter

public class CandidateStatusDTO {

    public CandidateStatusDTO() {
    }

    public CandidateStatusDTO(Long id, boolean status) {
        this.id = id;
        this.status = status;
    }

    private Long id;

    private boolean status;
}
