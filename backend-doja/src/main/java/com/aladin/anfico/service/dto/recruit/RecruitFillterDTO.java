package com.aladin.anfico.service.dto.recruit;

import com.aladin.anfico.domain.Recruit;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecruitFillterDTO {

    private Long id;
    private String titleVi;
}
