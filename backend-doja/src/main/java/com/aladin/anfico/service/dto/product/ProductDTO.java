package com.aladin.anfico.service.dto.product;

import com.aladin.anfico.domain.Product;
import lombok.*;

import javax.validation.constraints.Size;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

    private Long id;

    @Size(min = 0, max = 500)
    private String avatarUrl;

    private String descriptionEn;

    private String descriptionVi;

    @Size(min = 0, max = 500)
    private String titleEn;

    @Size(min = 0, max = 500)
    private String titleVi;

    private boolean priority;


    public ProductDTO(Product product){
        this.id = product.getId();
        this.descriptionEn = product.getDescriptionEn();
        this.descriptionVi= product.getDescriptionVi();
        this.avatarUrl= product.getAvatarUrl();
        this.titleEn = product.getTitleEn();
        this.titleVi = product.getTitleVi();
        this.priority = product.isPriority();
    }

}
