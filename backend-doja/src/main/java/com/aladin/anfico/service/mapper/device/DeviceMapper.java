package com.aladin.anfico.service.mapper.device;


import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.service.dto.topic.DeviceDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Topic} and its DTO {@link DeviceDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeviceMapper extends EntityMapper<DeviceDTO, Topic> {

}
