package com.aladin.anfico.service.mapper.project;

import com.aladin.anfico.domain.News;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.service.ProjectService;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectCreateDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link News} and its DTO {@link NewsCreateDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProjectMapper extends EntityMapper<ProjectCreateDTO, Project> {

}

