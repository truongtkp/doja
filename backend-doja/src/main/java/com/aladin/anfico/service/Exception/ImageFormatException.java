package com.aladin.anfico.service.exception;

public class ImageFormatException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ImageFormatException() {
        super("Image format is fail!");
    }
}
