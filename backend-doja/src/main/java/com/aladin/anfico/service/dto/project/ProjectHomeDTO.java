package com.aladin.anfico.service.dto.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.time.Instant;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectHomeDTO {

    private Long id;

    private String titleVi;

    private String titleEn;

    private String avatarUrl;

}
