package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.Candidate;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.repository.CandidateRepository;
import com.aladin.anfico.repository.RecruitRepository;
import com.aladin.anfico.service.FileService;
import com.aladin.anfico.service.RecruitService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.recruit.*;
import com.aladin.anfico.service.mapper.recruit.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RecruitServiceImpl implements RecruitService {

    private final Logger log = LoggerFactory.getLogger(RecruitServiceImpl.class);

    private final CandidateRepository candidateRepository;
    private final RecruitRepository recruitRepository;
    private final FileService fileServiceImpl;
    private final RecruitFilterMapper recruitFilterMapper;
    private final RecruitCreateMapper recruitCreateMapper;
    private final RecruitMapper recruitMapper;
    private final CandidateMapper candidateMapper;

    @Override
    public Optional<RecruitDTO> addRecruit(RecruitCreateDTO recruitCreateDTO){
        log.info("[Anfico App]: create information recruit with title {}", recruitCreateDTO.getTitleEn());
        Recruit recruit = recruitCreateMapper.toEntity(recruitCreateDTO);
        return Optional.of(recruitMapper.toDto(recruitRepository.save(recruit)));
    }
    @Override
    public Optional<RecruitDTO> updateRecruit(RecruitCreateDTO recruitCreateDTO){
       return
            Optional
                    .of(recruitRepository.findById(recruitCreateDTO.getId()))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(recruit -> {
                        if(!recruitCreateDTO.getAvatarPath().equals(recruit.getAvatarPath())) {
                            fileServiceImpl.deleteFile(recruit.getAvatarPath());
                        }
                        recruitCreateMapper.partialUpdate(recruit, recruitCreateDTO);
                        return recruitMapper.toDto(recruitRepository.save(recruit));
                    });
    }
    @Override
    public Optional<Recruit> deleteRecruit(Long id){
        return Optional
                .of(recruitRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(recruit -> {
                    fileServiceImpl.deleteFile(recruit.getAvatarPath());
                    recruitRepository.delete(recruit);
                    return recruit;
                });
    }
    @Override
    public HashMap<String, ?> getAllManagedRecruit(int page, int size) {
        log.info("-- Get all Recruit --");
        return
            ArrayDTO.convertListToSet(
                recruitRepository.count(),
                recruitMapper.toDto(recruitRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))).toList())
            );
    }
    @Override
    public List<RecruitFillterDTO> getAllFillterRecruit() {
        log.info("-- Get all fillter Recruit --");
        return recruitFilterMapper.toDto(recruitRepository.getAllRecruit());
    }
    @Override
    public Optional<Recruit> getRecruitById(Long id){
        return Optional
                .of(recruitRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
    @Override
    public HashMap<String, ?> getCandidateByRecruit(Long id, int page, int size){
        log.info("Get one recruit and candidate");
        return ArrayDTO.convertListToSet(
            candidateRepository.getCountByRecruit(id),
            candidateRepository.getCandidateByRecruitId(id, PageRequest.of(page,size, Sort.by(Sort.Direction.DESC, "id")))
        );
    }
    @Override
    public Page<Candidate> findAllCandidateByRecruitId(Long id, Pageable pageable) {
        return candidateRepository.findAllByRecruitId(id, pageable);
    }
    @Override
    public Optional<Candidate> getCandidateById(Long id){
        log.info("Get one recruit and candidate");
        return Optional
                .of(candidateRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
    @Override
    public HashMap<String, ?> getCandidate(int page, int size){
        log.info("Get one recruit and candidate");
        return ArrayDTO.convertListToSet(
            candidateRepository.count(),
            candidateRepository.findAll(PageRequest.of(page,size)).getContent()
        );
    }
    @Override
    public Optional<Candidate> createCandidateByRecruit( Recruit recruit , CandidateDTO candidateDTO){
        Candidate candidate = candidateMapper.toEntity(candidateDTO);
        candidate.setStatus(false);
        candidate.setRecruit(recruit);
        return Optional.of(candidateRepository.save(candidate));
    }
    @Override
    public  Optional<Candidate> isStatus(CandidateStatusDTO candidateStatusDTO){
       return Optional
                .of(candidateRepository.findById(candidateStatusDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(candidate -> {
                    candidate.setStatus(!candidateStatusDTO.isStatus());
                    return candidateRepository.save(candidate);
                });
    }
    @Override
    public HashMap<String, ?> searchRecruitEn(String param, int page, int size) {
        log.info("-- Get all Recruit --");
        return
                ArrayDTO.convertListToSet(
                        recruitRepository.countSearchEn(param),
                        recruitMapper.toDto(recruitRepository.searchRecruitsByAllEn(param,PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))))
                );
    }
    @Override
    public HashMap<String, ?> searchRecruitVi(String param, int page, int size) {
        log.info("-- Get all Recruit --");
        return
                ArrayDTO.convertListToSet(
                        recruitRepository.countSearchVi(param),
                        recruitMapper.toDto(recruitRepository.searchRecruitsByAllVi(param,PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))))
                );
    }


}
