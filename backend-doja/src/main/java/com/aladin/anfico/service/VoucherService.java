package com.aladin.anfico.service;

import com.aladin.anfico.domain.Voucher;

public interface VoucherService {
    Voucher addVoucher(Voucher booking);
}
