package com.aladin.anfico.service.dto.paypal;


import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.HttpClient;

@Getter
@Setter
public class HttpResponse {
    HttpClient meta;
    byte[] responseBody;

}
