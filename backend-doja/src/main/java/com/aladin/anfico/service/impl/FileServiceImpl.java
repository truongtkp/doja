package com.aladin.anfico.service.impl;

import com.aladin.anfico.service.FileService;
import com.aladin.anfico.service.dto.file.FileDTO;
import com.aladin.anfico.service.dto.file.ImageDTO;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;


@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final Logger log = LoggerFactory.getLogger(FileServiceImpl.class);

    @Value("${image.host-port}")
    private String hostPort;

    /**
     * uplodd img on server.
     *
     * @param file type (MultipartFile[]
     * @return HashMap<String, ?>.
     */
    @Override
    public HashMap<String, ?> uploadImage(MultipartFile[] file) throws IOException {
        String path = System.getProperty("user.dir")+"/image/";

        if(!Files.exists(Paths.get(path))){
            Files.createDirectories(Paths.get(path));
        }
        List<ImageDTO> images = new ArrayList<>();
        for (MultipartFile img : file){
            String nameSubString = img.getOriginalFilename().length() > 50 ? img.getOriginalFilename().substring(img.getOriginalFilename().length()-40) :
                img.getOriginalFilename();
            String nameImage = System.currentTimeMillis()+"の"+ nameSubString.replaceAll(" ","");
            img.transferTo(new File(path+nameImage));
            images.add(new ImageDTO(path + nameImage, hostPort + "/image/" + nameImage));
            log.info("[Anfico App]: Upload image {} on server", nameImage );
        }
        return ArrayDTO.convertListToSet(images.size(),images);
    }

    /**
     * Upload file on server.
     *
     * @param file type MultipartFile
     * @return FileDTO.
     */
    @Override
    public FileDTO uploadFile(MultipartFile file) throws IOException {
        String path = System.getProperty("user.dir")+"/file/";

        if(!Files.exists(Paths.get(path))){
            Files.createDirectories(Paths.get(path));
        }
        String namefile = System.currentTimeMillis()+"の"+file.getOriginalFilename().replaceAll(" ","");
        file.transferTo(new File(path+namefile));
        log.info("[Anfico App]: Upload file {} on server", namefile );
        return new FileDTO(hostPort + "/file/" + namefile, path + namefile);
    }

    /**
     * Download file on server.
     *
     * @param param type String
     * @return Resource.
     */
    @Override
    public Resource downloadFile(String param) throws IOException {
        ByteArrayResource resource = null;
        try {
            File file = new File(param);
            Path path = Paths.get(file.getAbsolutePath());
            resource = new ByteArrayResource(Files.readAllBytes(path));
            return resource;
        }catch (Exception e){
            return null;
        }
    }


    /**
     * Delete file on server.
     *
     * @param pathFile type String
     * @return Resource.
     */
    @Override
    public boolean deleteFile (String pathFile){
        Path path = Paths.get(pathFile);
        try {
            Files.delete(path);
            log.info("[Anfico App]: File or directory deleted successfully" );
            return true;
        } catch (NoSuchFileException ex) {
            log.info("[Anfico App]: No such file or directory {}", path );
            return false;
        } catch (DirectoryNotEmptyException ex) {
            log.info("[Anfico App]: Directory is not empty {}", path );
            return false;
        } catch (IOException ex) {
            log.info(String.valueOf(ex));
            return false;
        }
    }
}
