package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.Contact;
import com.aladin.anfico.repository.ContactRepository;
import com.aladin.anfico.service.ContactService;
import com.aladin.anfico.service.dto.contact.ContactCreateDTO;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.contact.ContactUpdateDTO;
import com.aladin.anfico.service.mapper.contact.ContactCreateMapper;
import com.aladin.anfico.service.mapper.contact.ContactUpdateMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService {

    private final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);
    private final ContactRepository contactRepository;
    private final ContactCreateMapper contactCreateMapper;
    private final ContactUpdateMapper contactUpdateMapper;
    private final MailService mailService;

    @Override
    public Optional<ContactCreateDTO> addContact(ContactCreateDTO contactCreateDTO){
        log.info("[Anfico App]: Create a contact on table Contact with fullname {}", contactCreateDTO.getFullname());
        return Optional.of(contactCreateMapper.toDto(contactRepository.save(contactCreateMapper.toEntity(contactCreateDTO))));
    }
    @Override
    public HashMap<String, ?> getAllContact(int page, int size){
        return ArrayDTO.convertListToSet(contactRepository.count(),
            contactRepository.getContact(PageRequest.of(page, size, Sort.by(Sort.Direction.ASC,  "status"))));
    }
    @Override
    public  Optional<Contact> getContactById(Long id){
        log.info("[Anfico App]: get a contact on table Contact with id {}", id);
        return
            Optional
                .of(contactRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
    @Override
    public Optional<Contact> deleteContactById(Long id){

        return
            Optional
                .of(contactRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(contact -> {
                    contactRepository.delete(contact);
                    log.info("[Anfico App]: Delete a contact on table Contact with id {}", id);
                    return contact;
                });
    }
    @Override
    public Optional<Contact> updateContact(ContactUpdateDTO contactUpdateDTO){
        return
            Optional
                    .of(contactRepository.findById(contactUpdateDTO.getId()))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                            .map(contact -> {
                                contactUpdateMapper.partialUpdate(contact, contactUpdateDTO);
                                if (!contact.isStatus()){
                                    String htmlMsg = "<!DOCTYPE html>" +
                                        "<html lang='en'>" +
                                        "" +
                                        "<head>" +
                                        "    <meta charset='UTF-8'>" +
                                        "    <meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
                                        "    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                                        "    <title>Document</title>" +
                                        "    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">" +
                                        "    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>" +
                                        "    <link href=\"https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,500;0,600;1,500&display=swap\" rel=\"stylesheet\">" +
                                        "    <style>" +
                                        "        *{" +
                                        "            font-family: 'Bai Jamjuree', sans-serif;" +
                                        "    font-style: normal;" +
                                        "    margin: 0;" +
                                        "    padding:0;" +
                                        "    box-sizing: border-box;" +
                                        "}" +
                                        "" +
                                        ":root {" +
                                        "    --primary: #003B72;" +
                                        "    --border-color: #DBDFE4;" +
                                        "}" +
                                        "" +
                                        ".container{" +
                                        "    margin-top: 50px;" +
                                        "    width: 600px;" +
                                        "    margin: auto;" +
                                        "    margin-top: 100px;" +
                                        "    padding: 14px 0;" +
                                        "}" +
                                        "" +
                                        "header {" +
                                        "    display: flex;" +
                                        "    padding: 0 14px 0 40px;" +
                                        "    justify-content: space-between;" +
                                        "}" +
                                        "" +
                                        "" +
                                        "" +
                                        "header .logo img{" +
                                        "   width: 134px;" +
                                        "   object-fit: cover;" +
                                        "}" +
                                        "" +
                                        "header .infomation {" +
                                        "    margin-left: auto;" +
                                        "    width: 310px;" +
                                        "}" +
                                        "" +
                                        "header .infomation p{" +
                                        "    text-align: left;" +
                                        "    margin: 0;" +
                                        "" +
                                        "}" +
                                        "header .infomation p.name{" +
                                        "    font-weight: 500;" +
                                        "    font-size: 15px;" +
                                        "    line-height: 17px;" +
                                        "    color: #003B72;" +
                                        "}" +
                                        "header .infomation p.address{" +
                                        "    font-weight: 400;" +
                                        "    font-size: 15px;" +
                                        "    margin: 9px 0;" +
                                        "    line-height: 15px;" +
                                        "    color: #616161;" +
                                        "}" +
                                        "" +
                                        "header .infomation p.phone{" +
                                        "    font-weight: 400;" +
                                        "    font-size: 13px;" +
                                        "    line-height: 15px;" +
                                        "    color: #616161;" +
                                        "}" +
                                        "section .title{" +
                                        "    margin-top: 81px;" +
                                        "    " +
                                        "    padding-bottom: 40px;" +
                                        "" +
                                        "}" +
                                        "section .title p{" +
                                        "    font-size: 20px;" +
                                        "    font-weight: 700;" +
                                        "    line-height: 39px;" +
                                        "    text-align: center;" +
                                        "    text-transform: uppercase;" +
                                        "    color: #003B72" +
                                        "}" +
                                        "" +
                                        "section .title .line{" +
                                        "    border-bottom: 2px solid #003B72;" +
                                        "    width: 100%;" +
                                        "    margin: 0 auto;" +
                                        "    margin-top: 12px;" +
                                        "}" +
                                        "section .content{" +
                                        "    margin: 0 40px;" +
                                        "    width: auto;" +
                                        "    height: auto;" +
                                        "    border-radius: 10px;" +
                                        "    overflow: hidden;" +
                                        "    border: 2px solid #DBDFE4;" +
                                        "}" +
                                        "section .content table p {" +
                                        "    /* padding: 26px 210px 26px 23px; */" +
                                        "    font-size: 16px;" +
                                        "    text-align: start;" +
                                        "}" +
                                        " td, th {" +
                                        "    border-width: 2px;" +
                                        "    border-style: solid;" +
                                        "    border-color: transparent #DBDFE4 #DBDFE4 transparent;" +
                                        "    /* border: 2px solid #DBDFE4; */" +
                                        "   " +
                                        "}" +
                                        "" +
                                        "td {" +
                                        "    border-right: none;" +
                                        "}" +
                                        "" +
                                        "table tr:last-child th, table tr:last-child td {" +
                                        "    border-bottom: none;" +
                                        "}" +
                                        "" +
                                        "table tr th, table tr td {" +
                                        "    padding: 26px 24px;" +
                                        "}" +
                                        "" +
                                        "" +
                                        "table tr th p {" +
                                        "    width: max-content;" +
                                        "    max-width: 150px;" +
                                        "}" +
                                        "" +
                                        "" +
                                        "" +
                                        "table th {" +
                                        "    font-weight: 500;" +
                                        "    width: 100px;" +
                                        "    color: #003B72;" +
                                        "    text-transform: uppercase;" +
                                        "}" +
                                        "table td {" +
                                        "    font-weight: 400;" +
                                        "    color: #393939;" +
                                        "}" +
                                        "  " +
                                        "table {" +
                                        "    width: 100%;" +
                                        "    border-collapse: collapse ;" +
                                        "    " +
                                        "}" +
                                        "" +
                                        "section .describe {" +
                                        "    padding: 0 40px;" +
                                        "}" +
                                        "" +
                                        "section .describe .question .label {" +
                                        "    font-weight: 500;" +
                                        "    margin-bottom: 12px;" +
                                        "    margin-top: 22px;" +
                                        "    font-size: 16px;" +
                                        "    color: #003B72;" +
                                        "    text-transform: uppercase;" +
                                        "}" +
                                        "section .describe .question textarea{" +
                                        "    height: 65px;" +
                                        "    width: 100%;" +
                                        "    border-radius: 10px;" +
                                        "    outline: none;" +
                                        "\tfont-size: 16px;" +
                                        "    border: 1px solid #DBDFE4;" +
                                        "    padding: 4px;" +
                                        "}" +
                                        "" +
                                        "footer{" +
                                        "    margin-top: 28px;" +
                                        "    margin-bottom: 45px;" +
                                        "}" +
                                        "" +
                                        "footer p{" +
                                        "    font-weight: 500;" +
                                        "    font-size: 16px;" +
                                        "    line-height: 19px;" +
                                        "    text-align: center;" +
                                        "    color: #003B72;" +
                                        "}" +
                                        "" +
                                        "" +
                                        "    </style>" +
                                        "</head>" +
                                        "" +
                                        "<body>" +
                                        "    <div class='container'>" +
                                        "        <header>" +
                                        "            <div class='logo'>" +
                                        "                <img class=\"logo_img\" src='https://anfico.vn/image/logo.png'" +
                                        "                    alt='logo'>" +
                                        "            </div>" +
                                        "" +
                                        "            <div class='infomation'>" +
                                        "                <p class='name'> An Vuong Food Industry Equipment Co., Ltd</p>" +
                                        "                <p class='address'>B5-26 Vinhomes Gardenia Ham Nghi Cau Dien, Nam Tu Liem, Hanoi</p>" +
                                        "                <p class='phone'>SĐT: +84 (226) 3537228 / +84 (226) 353 7229</p>" +
                                        "            </div>" +
                                        "" +
                                        "        </header>" +
                                        "        <section>" +
                                        "            <div class='title'>" +
                                        "                <p>request feedback consultation</p>" +
                                        "                <div class='line'></div>" +
                                        "            </div>" +
                                        "            <div class='content'>" +
                                        "                <table>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p >Full name</p>" +
                                        "                        </th>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getFullname()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p>Phone number</p>" +
                                        "                        </th>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getPhone()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p>Company name</p>" +
                                        "                            </td>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getCompanyName()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p>Company address</p>" +
                                        "                        </th>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getCompanyAddress()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p>Email</p>" +
                                        "                        </th>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getEmail()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "                    <tr>" +
                                        "                        <th>" +
                                        "                            <p>Where do you know Anfico from?</p>" +
                                        "                        </th>" +
                                        "                        <td>" +
                                        "                            <p>"+contact.getFromTo()+"</p>" +
                                        "                        </td>" +
                                        "                    </tr>" +
                                        "" +
                                        "                </table>" +
                                        "            </div>" +
                                        "            <div class='describe'>" +
                                        "                <div class='question'>" +
                                        "                    <p class='label'>Content to be consulted</p>" +
                                        "                    <textarea readonly>"+contact.getNote()+"</textarea>" +
                                        "                </div>" +
                                        "                <div class='question'>" +
                                        "                    <p class='label'>Content of customer consultation</p>" +
                                        "                    <textarea> "+contact.getFeedback()+"</textarea>" +
                                        "                </div>" +
                                        "            </div>" +
                                        "" +
                                        "        </section>" +
                                        "        <footer>" +
                                        "            <p>Thank you for using our service!</p>" +
                                        "            <p>Anfico</p>" +
                                        "        </footer>" +
                                        "    </div>" +
                                        "" +
                                        "</body>" +
                                        "" +
                                        "</html>";
                                    try {
                                        mailService.sendEmail(contact.getEmail(),htmlMsg,"Feedback to anfico");
                                    } catch (MessagingException e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                                contact.setStatus(true);
                                return contactRepository.save(contact);
                            });
    }
    @Override
    public HashMap<String, ?> searchContactByAll(String param,int page, int size){
        return ArrayDTO.convertListToSet(contactRepository.countSearch(param),
                contactRepository.searchContactsByAll(param, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))));
    }



}
