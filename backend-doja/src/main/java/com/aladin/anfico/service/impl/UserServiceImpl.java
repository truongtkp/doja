package com.aladin.anfico.service.impl;

import com.aladin.anfico.config.KeycloakConfiguration;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.security.SecurityUtils;
import com.aladin.anfico.service.UserService;
import java.util.*;
import java.util.stream.Collectors;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.user.*;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.ws.rs.core.Response;

/**
 * Service class for managing users.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private final FileServiceImpl fileServiceImpl;
    private final KeycloakConfiguration keycloakConfiguration;
    @Value("${image.url}")
    private String avatarUrl;
    @Value("${image.path}")
    private String avatarPath;
    @Value("${keycloak.realm}")
    private String realme;

    @Value("${keycloak.resource}")
    private String resource;

//    private static  HashMap<String,?> hashMap = null;
    private static  int countUser = 0;

//    private static  List<UserKeycloakDTO> listUserKeycloakSearch = null;
    private static  int countUserSearch = 0;

    /**
     * {@Create} information in user table.
     *
     * @param userCreateDTO to DTO
     * @return Optional<User>.
     */
    @Override
    public UserKeycloakDTO addUser(UserCreateDTO userCreateDTO){

        try{
            Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
            RealmResource realmResource = keycloak.realm(realme);
            UsersResource userRessource = realmResource.users();
            log.info("[Anfico App]: create user in user table login = {}", userCreateDTO.getLogin());
            UserRepresentation userRepresentation = userCreateDTO.createUserRepresentation();
            userRepresentation.getAttributes().put("avatarUrl", Collections.singletonList(avatarUrl));
            userRepresentation.getAttributes().put("avatarPath", Collections.singletonList(avatarPath));
            Response response = userRessource.create(userRepresentation);

            String userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(userCreateDTO.getPassword());
            userRessource.get(userId).resetPassword(passwordCred);
            RoleRepresentation testerRealmRole = realmResource.roles()
                .get(AuthoritiesConstants.USER).toRepresentation();
            userRessource.get(userId).roles().realmLevel()
                .add(Arrays.asList(testerRealmRole));
            ClientRepresentation client = realmResource.clients() //
                .findByClientId(resource).get(0);
            RoleRepresentation userClientRole = realmResource.clients().get(client.getId()) //
                .roles().get(AuthoritiesConstants.USER).toRepresentation();
            userRessource.get(userId).roles().clientLevel(client.getId()).add(Arrays.asList(userClientRole));
            UserKeycloakDTO userKeycloakDTO = new UserKeycloakDTO(userRessource.get(userId).toRepresentation(), Arrays.asList(userClientRole));
            keycloak.close();
            return userKeycloakDTO;
        }catch (Exception e){
            return null;
        }
    }


    /**
     * {@Read} information in user table.
     *
     * @param page type int
     * @param size type int
     * @return Optional<User>.
     */
    @Override
    @Transactional(readOnly = true)
    public HashMap<String,?> getAllManagedUsers(int page, int size) {
        log.info("[Anfico App]: get all user in user table page = {}, size = {}", page, size);
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        RealmResource realmResource = keycloak.realm(realme);
        UsersResource userKeycloak = realmResource.users();
        int count = userKeycloak.count();
//        if (this.countUser == 0 || countUser !=  count){
        List<UserKeycloakDTO> listUserKeycloaks = new ArrayList<>();
        userKeycloak.list(page*size, size).forEach(userRepresentation -> {
//            get client
            ClientRepresentation clientRepresentation = realmResource.clients().findByClientId(resource).get(0);
            UserResource userKeycloakId = userKeycloak.get(userRepresentation.getId());
            List<RoleRepresentation> roles = userKeycloakId.roles().clientLevel(clientRepresentation.getId()).listAll();
            listUserKeycloaks.add(new UserKeycloakDTO(userRepresentation, roles));
        });
//        countUser = count;



//            hashMap =
//                ArrayDTO.paginationList(listUserKeycloaks.stream()
//                    .sorted((o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate())).collect(Collectors.toList()), page, size);
//            keycloak.close();
//        }

        return ArrayDTO.convertListToSet(count, listUserKeycloaks);
    }


    /**
     * {@Update} information in user table.
     *
     * @param userUpdateDTO to DTO
     * @return Optional<User>.
     */
    @Override
    public Optional<UserKeycloakDTO> updateUser(UserUpdateDTO userUpdateDTO) {
        try {
            Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
            RealmResource realmResource = keycloak.realm(realme);
            UsersResource userRessource = realmResource.users();
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(userUpdateDTO.getPassword());
            userRessource.get(userUpdateDTO.getId()).resetPassword(passwordCred);
//          delete image
            UserRepresentation userRepresentation =  userRessource.get(userUpdateDTO.getId()).toRepresentation();
            userRessource.get(userUpdateDTO.getId()).update(userUpdateDTO.createUserRepresentation(userRepresentation));

            UserResource userResource = keycloak.realm(realme).users().get(userRepresentation.getId());
            ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
            List<RoleRepresentation> roles = userResource.roles().clientLevel(clientRepresentation.getId()).listAll();


            Optional<UserKeycloakDTO>  userKeycloakDTO = Optional.of(new UserKeycloakDTO(userRessource.get(userUpdateDTO.getId()).toRepresentation(), roles));
            keycloak.close();
            return userKeycloakDTO;
        }catch (Exception e){
            return null;
        }

    }

    /**
     * {@Delete} information in user table.
     *
     * @param id type String
     * @return Optional<User>.
     */
    @Override
    public boolean deleteUser(String id){
         log.info("[Anfico App]: delete user with login {}", id);
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        try{
            UserResource userResource = keycloak.realm(realme).users().get(id);
            ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
            List<RoleRepresentation> roles = userResource.roles().clientLevel(clientRepresentation.getId()).listAll();
            roles.forEach(roleRepresentation -> {
                if (roleRepresentation.getName().equals(AuthoritiesConstants.ADMIN) || roleRepresentation.getName().equals("ROLE_SYSTEM"))
                    throw new BadRequestAlertException("role is admin !", "", "role");
            });
            keycloak.realm(realme).users().get(id).remove();
            countUser = 0;
            countUserSearch = 0;
            keycloak.close();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Transactional
    @Override
    public boolean changePassword(PasswordChangeDTO passwordChangeDTO, String userId) {
            Keycloak keycloak = keycloakConfiguration.getKeycloakInstanceUser(SecurityUtils.getCurrentUserLogin().get(),
                passwordChangeDTO.getCurrentPassword());
            try{
                RealmResource realmResource = keycloak.realm(realme);
                realmResource.users().get(userId).toRepresentation();
                UserResource userResource = keycloak.realm(realme).users().get(userId);
                ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
                List<RoleRepresentation> roles = userResource.roles().clientLevel(clientRepresentation.getId()).listAll();
                roles.forEach(roleRepresentation -> {
                    if (roleRepresentation.getName().equals("ROLE_SYSTEM")
                    ) throw new BadRequestAlertException("","","");
                });
                keycloak.close();
            }catch (Exception e){
                keycloak.close();
                return false;
            }

            Keycloak keycloak_new = keycloakConfiguration.getKeycloakInstance();
            RealmResource realmResource = keycloak_new.realm(realme);
            UsersResource userRessource = realmResource.users();
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(passwordChangeDTO.getNewPassword());
            userRessource.get(userId).resetPassword(passwordCred);
            return true;
    }


    @Override
    public Optional<UserKeycloakDTO> updatePublicUser(UserPublicDTO userUpdateDTO){
        try {
            Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
            RealmResource realmResource = keycloak.realm(realme);
            UsersResource userRessource = realmResource.users();
            UserRepresentation userRepresentation =  userRessource.get(userUpdateDTO.getId()).toRepresentation();

            if (userRepresentation.getAttributes() != null){
                if (userRepresentation.getAttributes().containsKey("avatarPath")){
                    if (!userRepresentation.getAttributes().get("avatarPath").get(0).equals(avatarPath)
                        && !userUpdateDTO.getAvatarPath().equals(userRepresentation.getAttributes().get("avatarPath").get(0))){
                        fileServiceImpl.deleteFile(userRepresentation.getAttributes().get("avatarPath").get(0));
                    }
                }
            }

            userRessource.get(userUpdateDTO.getId()).update(userUpdateDTO.createUserRepresentation(userRepresentation));

            UserResource userResource = keycloak.realm(realme).users().get(userRepresentation.getId());
            ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
            List<RoleRepresentation> roles = userResource.roles().clientLevel(clientRepresentation.getId()).listAll();

            Optional<UserKeycloakDTO>  userKeycloakDTO = Optional.of(new UserKeycloakDTO(userRessource.get(userUpdateDTO.getId()).toRepresentation(), roles));
            keycloak.close();
            return userKeycloakDTO;
        }catch (Exception e){
            return null;
        }
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<UserKeycloakDTO> getUserWithAuthoritiesByLogin(String id) {
        log.info("[Anfico App]: get detail user with login {}", id);
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        RealmResource realmResource = keycloak.realm(realme);
        UsersResource userRessource = realmResource.users();

        ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
        List<RoleRepresentation> roles = userRessource.get(id).roles().clientLevel(clientRepresentation.getId()).listAll();

        Optional<UserKeycloakDTO> keycloakDTO =  Optional.of(new UserKeycloakDTO(userRessource.get(id).toRepresentation(), roles));
        keycloak.close();
        return keycloakDTO;
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<UserKeycloakDTO> getUserWithAuthorities(String userId) {
        log.info("[Anfico App]: get user by token");
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        RealmResource realmResource = keycloak.realm(realme);
        UsersResource userRessource = realmResource.users();
        ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
        List<RoleRepresentation> roles = userRessource.get(userId).roles().clientLevel(clientRepresentation.getId()).listAll();
        return Optional.of(new UserKeycloakDTO(userRessource.get(userId).toRepresentation(), roles));
    }

    @Override
    public boolean activeUser(String userId) {
        log.info("[Anfico App]: active user by token");
        try{
            Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
            RealmResource realmResource = keycloak.realm(realme);
            UsersResource userRessource = realmResource.users();
            UserRepresentation userRepresentation =  userRessource.get(userId).toRepresentation();


            UserResource userResource = keycloak.realm(realme).users().get(userRepresentation.getId());
            ClientRepresentation clientRepresentation = keycloak.realm(realme).clients().findByClientId(resource).get(0);
            List<RoleRepresentation> roles = userResource.roles().clientLevel(clientRepresentation.getId()).listAll();

            if (roles.size() == 0){
                RoleRepresentation testerRealmRole = realmResource.roles().get(AuthoritiesConstants.USER).toRepresentation();
                userRessource.get(userId).roles().realmLevel().add(Arrays.asList(testerRealmRole));
                ClientRepresentation client = realmResource.clients().findByClientId(resource).get(0);
                RoleRepresentation userClientRole = realmResource.clients().get(client.getId()).roles().get(AuthoritiesConstants.USER).toRepresentation();
                userRessource.get(userId).roles().clientLevel(client.getId()).add(Arrays.asList(userClientRole));
                return true;
            }else{
                roles.forEach(roleRepresentation -> {
                    if (roleRepresentation.getName().equals(AuthoritiesConstants.ADMIN) || roleRepresentation.getName().equals("ROLE_SYSTEM"))
                        throw new BadRequestAlertException("role is admin !", "", "role");
                });
            }
            userRepresentation.setEnabled(!userRepresentation.isEnabled());
            userRessource.get(userId).update(userRepresentation);
            this.countUser = 0;
            this.countUserSearch = 0;
            keycloak.close();
            return userRepresentation.isEnabled();
        }catch (Exception e){
            throw new BadRequestAlertException("ID exist !", "", "id");
        }

    }

    /** {@Search}*/
    @Override
    @Transactional(readOnly = true)
    public HashMap<String, ?> searchUser(String param, int page, int size){
        log.info("[Anfico App]: search user by param {}", param);
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        RealmResource realmResource = keycloak.realm(realme);
        UsersResource userKeycloak = realmResource.users();
        List<UserKeycloakDTO> listUserKeycloakSearch = new ArrayList<>();
        userKeycloak.search(param, page*size, page*size + size).forEach(userRepresentation -> {
                ClientRepresentation clientRepresentation = realmResource.clients().findByClientId(resource).get(0);
                List<RoleRepresentation> roles = userKeycloak.get(userRepresentation.getId()).roles().clientLevel(clientRepresentation.getId()).listAll();
                listUserKeycloakSearch.add(new UserKeycloakDTO(userRepresentation, roles));
            });
        HashMap<String,?> hashMap = ArrayDTO.convertListToSet(listUserKeycloakSearch.size(), listUserKeycloakSearch);
        keycloak.close();
        return hashMap;
    }


    @Override
    public boolean findOneByLoginIgnoreCase(String str){
        Keycloak keycloak = keycloakConfiguration.getKeycloakInstance();
        RealmResource realmResource = keycloak.realm(realme);
        UsersResource userRessource = realmResource.users();
        Optional<UserRepresentation> userRepresentations = userRessource.search(str)
            .stream().filter(userRepresentation -> userRepresentation.getUsername().equals(str)).findFirst();
        if (userRepresentations.isPresent()){
            keycloak.close();
            return false;
        }
        keycloak.close();
        return true;
    }

}
