package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.Category;
import com.aladin.anfico.repository.CategoryRepository;
import com.aladin.anfico.service.CategoryService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.category.CategoryDTO;
import com.aladin.anfico.service.mapper.category.CategoryMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);
    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;


    /**
     * {@Create} information in category table.
     *
     * @param categoryDTO to DTO
     * @return  List<Category>
     */
    @Override
    public Category  addCategory(CategoryDTO categoryDTO){
        log.info("[Anfico App]: add {} category", categoryDTO.getNameEn());
        Category category = categoryMapper.toEntity(categoryDTO);
        return categoryRepository.save(category);
    }

    /**
     * {@Update} information in category table.
     *
     * @param categoryDTO to DTO
     * @return  Category
     */
    @Override
    public Optional<Category> updateCategory(CategoryDTO categoryDTO){
        return
            Optional
                .of(categoryRepository.findById(categoryDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(category -> {
                    categoryMapper.partialUpdate(category, categoryDTO);
                    log.info("[Anfico App]: update {} category", category.getId());
                    return categoryRepository.save(category);
                });
    }
    /**
     * {@Delete} information in category table.
     *
     * @param id type Long
     * @return boolean
     */
    @Override
    public Optional<Category> deleteCategory(Long id){
        return
            Optional
                .of(categoryRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(category -> {
                    log.info("[Anfico App]: delete category {} success", category.getNameEn());
                     categoryRepository.delete(category);
                     return category;
                });
    }

    /**
     * {@Read} information in category table.
     * @return  HashMap<String,?>
     */
    @Override
    public HashMap<String,?> getAllManagedCategory() {
        log.info("[Anfico App]: get all category");
        return ArrayDTO.convertListToSet(categoryRepository.count(), categoryRepository.findAll());
    }
}
