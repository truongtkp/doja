package com.aladin.anfico.service;

import com.aladin.anfico.domain.Staff;

import java.util.List;

public interface StaffService {
    List<Staff> getAllStaff();
    Staff addStaff(Staff staff);
}
