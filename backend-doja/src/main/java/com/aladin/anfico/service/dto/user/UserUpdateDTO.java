package com.aladin.anfico.service.dto.user;

import com.aladin.anfico.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.keycloak.representations.idm.UserRepresentation;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateDTO {

    @Id
    @NotNull
    private String id;

    @Pattern(regexp = Constants.PASS_REGEX)
    @NotNull
    private String password;

    @NotNull
    private String fullname;


    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;

    @NotNull
    private Date birth;

    @NotNull
    private boolean gender;

    @NotNull
    private String position;

    public UserRepresentation createUserRepresentation( UserRepresentation userRepresentation){
        userRepresentation.setFirstName(getFullname());
        userRepresentation.setEnabled(true);
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if (userRepresentation.getAttributes() == null)
            attributes= new HashMap<>();
        attributes.put("phone", Collections.singletonList(getPhone()));
        attributes.put("birth", Collections.singletonList(String.valueOf(getBirth().getTime())));
        attributes.put("position", Collections.singletonList(getPosition()));
        attributes.put("gender" , Collections.singletonList(String.valueOf(isGender())));

        userRepresentation.setAttributes(attributes);

        return userRepresentation;
    }

}
