package com.aladin.anfico.service;

import com.aladin.anfico.domain.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAllAddresses();

}
