package com.aladin.anfico.service.mapper.recruit;

import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.RecruitCreateDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Recruit} and its DTO {@link RecruitCreateDTO}.
 */
@Mapper(componentModel = "spring")
public interface RecruitCreateMapper extends EntityMapper<RecruitCreateDTO, Recruit> {

}

