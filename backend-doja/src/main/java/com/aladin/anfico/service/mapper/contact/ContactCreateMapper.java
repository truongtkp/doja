package com.aladin.anfico.service.mapper.contact;

import com.aladin.anfico.domain.Contact;
import com.aladin.anfico.service.dto.contact.ContactCreateDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Contact} and its DTO {@link ContactCreateDTO}.
 */
@Mapper(componentModel = "spring")
public interface ContactCreateMapper extends EntityMapper<ContactCreateDTO, Contact> {

}
