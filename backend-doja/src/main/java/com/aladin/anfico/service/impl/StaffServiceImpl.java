package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.Staff;
import com.aladin.anfico.repository.StaffRepository;
import com.aladin.anfico.service.StaffService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StaffServiceImpl implements StaffService {
    private final Logger log = LoggerFactory.getLogger(StaffServiceImpl.class);
    private final StaffRepository staffRepository;

    @Override
    public List<Staff> getAllStaff(){
        log.info("[Doja App]: get all staff member");
        return staffRepository.findAll();
    }

    @Override
    public Staff addStaff(Staff staff) {
        return staffRepository.save(staff);
    }
}
