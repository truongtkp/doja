package com.aladin.anfico.service.dto.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class SpecificationDTO {

    private Long id;

    @Size(max = 256)
    private String nameEn;

    @Size(max = 256)
    private String nameVi;

    List<SpecificationDetailDTO> specificationDetailList;

}
