package com.aladin.anfico.service;

import com.aladin.anfico.domain.Topic;
import com.aladin.anfico.domain.TopicImage;
import com.aladin.anfico.domain.Type;
import com.aladin.anfico.service.dto.topic.DeviceDTO;
import com.aladin.anfico.service.dto.topic.TopicDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TopicService {


    Optional<Set<TopicImage>> getTopicBanner(Type type);

    Optional<Set<TopicImage>> getTopicAlbum(Long id);

    HashMap<String, ?> addTopicImage(List<TopicImage> topicImages, Type type);

    void deleteTopicImage(Long id);

    Boolean deleteTopicImage(List<Long> id);

    Optional<List<DeviceDTO>> getAllTopic(Type type);

    DeviceDTO addDevice(DeviceDTO dto);

    Optional<DeviceDTO> updateDevice(DeviceDTO dto);

    HashMap<String, ?> getAllTopic(int page, int size);

    boolean deleteAlbum(Long id);

    boolean deleteDevice(Long id);

    HashMap<String, ?> addListAlbum(List<TopicImage> topicImages, Type type, Long id);

    Topic addTopic(Type type, TopicDTO topic);

    Topic updateTopic(TopicDTO topicDTO);

    HashMap<String, ?> searchDevice(String param, int page, int size);


}
