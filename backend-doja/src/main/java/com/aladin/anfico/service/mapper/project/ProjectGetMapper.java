package com.aladin.anfico.service.mapper.project;

import com.aladin.anfico.domain.News;
import com.aladin.anfico.domain.Project;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectCreateDTO;
import com.aladin.anfico.service.dto.project.ProjectDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link Project} and its DTO {@link ProjectDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProjectGetMapper extends EntityMapper<ProjectDTO, Project> {

}