package com.aladin.anfico.service.mapper.category;

import com.aladin.anfico.domain.Category;
import com.aladin.anfico.service.dto.category.CategoryDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Category} and its DTO {@link CategoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {

}