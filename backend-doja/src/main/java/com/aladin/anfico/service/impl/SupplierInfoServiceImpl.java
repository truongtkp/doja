package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.SupplierInfo;
import com.aladin.anfico.repository.SupplierInfoRepository;
import com.aladin.anfico.service.SupplierInfoService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.supplier.SupplierInfoDTO;
import com.aladin.anfico.service.mapper.supplierInfo.SupplierInfoMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierInfoServiceImpl implements SupplierInfoService {

    private final Logger log = LoggerFactory.getLogger(SupplierInfoServiceImpl.class);
    private final SupplierInfoRepository supplierInfoRepository;
    private final FileServiceImpl fileServiceImpl;

    private final SupplierInfoMapper supplierInfoMapper;

    @Override
    public SupplierInfoDTO addSupplier(SupplierInfoDTO supplierInfoDTO){
        log.info("--add a supplierInfo-- ");
        SupplierInfo supplierInfo = supplierInfoMapper.toEntity(supplierInfoDTO);
        return supplierInfoMapper.toDto(supplierInfoRepository.save(supplierInfo));
    }
    @Override
    public Optional<SupplierInfo> updateSupplierInfo(SupplierInfoDTO supplierInfoDTO){
        return Optional
            .of(supplierInfoRepository.findById(supplierInfoDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(supplierInfo -> {
                if (!supplierInfo.getAvatarPath().equals(supplierInfoDTO.getAvatarPath())){
                    fileServiceImpl.deleteFile(supplierInfo.getAvatarPath());
                }
                supplierInfoMapper.partialUpdate(supplierInfo, supplierInfoDTO);

                log.info("--Update a supplierInfo--");
                return supplierInfoRepository.save(supplierInfo);
            });
    }
    @Override
    public Optional<SupplierInfo> deleteSupplierInfo(Long id){

        return Optional
                .of(supplierInfoRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(supplierInfo -> {
                    fileServiceImpl.deleteFile(supplierInfo.getAvatarPath());
                    supplierInfoRepository.delete(supplierInfo);
                    return supplierInfo;
                });
    }
    @Override
    public HashMap<String,?> getAllManagedSupplierInfo(int page, int size) {
        log.info("--Get all a history--");
        return ArrayDTO.convertListToSet(supplierInfoRepository.count(),supplierInfoRepository
            .findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))).toList());

    }
}
