package com.aladin.anfico.service.dto.contact;


import com.aladin.anfico.config.Constants;
import com.aladin.anfico.domain.Contact;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ContactCreateDTO {

    @Id
    private Long id;

    @NotNull
    @Size(min = 5, max = 256)
    private String fullname;

    @NotNull
    @Size(min = 5, max = 256)
    private String companyName;

    @NotNull
    @Size(min = 5, max = 256)
    private String companyAddress;

    @Email
    @NotNull
    private String email;

    @NotNull
    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;

    private String note;

    @Size(min = 0, max = 100)
    private String fromTo;

}
