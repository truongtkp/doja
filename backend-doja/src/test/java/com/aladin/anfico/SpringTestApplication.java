package com.aladin.anfico;

import com.aladin.anfico.domain.Address;
import com.aladin.anfico.service.dto.paypal.HttpResponse;
import com.aladin.anfico.service.impl.GoogleMapServiceImpl;
import com.aladin.anfico.service.impl.PaypalServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
public class SpringTestApplication {

    @Test
    public void testPaypal() {

        String bearerToken = "A21AAKXPiC1gNgTA83QlNLSdq5pOiXaOpZM47P0E_pVZNhmZ3Aj0Cehu5Iuz9JKnvv6W5Jmo4ZZnGAA0q_HHpXIpugnr2un2A";
        PaypalServiceImpl paypalService = new PaypalServiceImpl();
        try {
//            byte[] test = paypalService.getToken();

//            PaypalHttpResponse test = paypalService.createOrderV2(bearerToken);
//            PaypalHttpResponse test = paypalService.getOrderDetailV2(bearerToken,"8HY18640YR314842E");
            HttpResponse test = paypalService.generateBearerAccessToken();
//            PaypalHttpResponse test = paypalService.capturePaymentV2(bearerToken,"14F95876S4579264H");
//            PaypalHttpResponse test = paypalService.confirmPaymentSourceV2(bearerToken,"14F95876S4579264H");



//            System.out.println(new String(test.getResponseBody(), "US-ASCII"));
            System.out.println(new String(test.getResponseBody()));


            ObjectMapper om = new ObjectMapper();
            TypeReference<Map<String,Object>> tr = new TypeReference<>() {};
            Map<String,Object> val = om.readValue(test.getResponseBody(),tr);


            System.out.println(val);

        } catch (Exception e){
            e.printStackTrace();
        }


    }




    @Test
    public void googleMapApi(){

        String test = "55 Ph&#7889; Lê &#272;&#7841;i Hành, Lê &#272;&#7841;i Hành, Hai Bà Tr&#432;ng, Hà N&#7897;i";
        String result = StringEscapeUtils.unescapeHtml4(test);
        System.out.println(result);

    }

    @Test
    public void temp(){
        String test = "Ph&#7889; Ph&#7841;m Ng&#7885;c Th&#7841;ch, Trung T&#7921;, &#272;&#7889;ng &#272;a, Hà N&#7897;i";
        test = test.replaceAll("$#","/u");


        String vn = "có công mài sắt";
        byte [] encode = vn.getBytes();

        System.out.println(new String(encode));


    }

}
