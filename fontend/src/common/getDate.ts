export default function getDate(date:string | number) {
    const currentPage = new Date(date);
    const day = `00${currentPage.getDate()}`.slice(-2);
    const month = `00${currentPage.getMonth() + 1}`.slice(-2);
    const string = `${day}/${month}/${currentPage.getFullYear()}`
    return  string
}