import clsx from "clsx"
import useInView from "../../hooks/useInView"
import { hostBE } from "../../types/host"

export default function Banner () {
const {isInView, ref} = useInView()
    return <div ref={ref} className="w-full overflow-hidden h-[500px] flex justify-center sc>768:h-[200px] items-center bg-primary">
        <img src={`${hostBE}/fe/logo_footer.png`} alt="" className={clsx("h-full ", {"animate__animated  animate__fadeInDown":isInView} )} />
    </div>
}