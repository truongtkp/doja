import clsx from "clsx";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import PopUpForm  from "../Contact/PopupForm";
import { hostBE } from "../../types/host";
import { showModal } from "../../reducers/modal";
import { useAppDispatch } from "../../hooks/hook";

export default function ContactBottom() {

    const navigate = useNavigate();
    const [t] = useTranslation();
    const { ref, isInView } = useInView();
    const dispatch = useAppDispatch();

    const showModalContact = () => {
        dispatch(showModal(<PopUpForm />))
      };

    return (
        <div className="lssm:h-auto sc<992:h-[579px] max-w-full bg-[#C2B8AF] relative" ref={ref}>
            <div className="flex w-1920:flex-row 2xl:flex-row xl:flex-row lg:flex-row m992:flex-row md:flex-col sc>768:flex-col">
                <div className="w-full">
                    <img src={`${hostBE}/fe/home_conact_bg.png`} alt="contacer" className="h-full w-full max-w-full sc991:w-full object-cover" />
                </div>
                <div className="w-full m992:px-10 m992:py-10 sc991:px-10 sc991:py-10 xl:px-4 xl:py-4">
                    <div className="-mt-4 pr-0 w-full">
                        <div
                            className={clsx(
                                "flex flex-col 2xl:px-24 xl:px-8 lg:px-10 2xl:pt-12 xl:pt-4 items-end flex-wrap-reverse text-text-title font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
                                { "animate__animated animate__fadeInRight": isInView }
                            )}
                        >
                            <img src={`${hostBE}/fe/icon-topic2.png`} className="2xl:w-[234px] xl:w-[110px] lg:w-[90px] md:w-[100px] sc991:w-[80px] lg:pt-8 w-[70px]" alt="icon-topic2.png" />
                            <span className="Valky 2xl:pb-[78px] xl:pb-[40px] 2xl:pt-10 sc991:pb-2 pb-6 text-[#FFFFFF] md:text-[50px] 2xl:text-[128px] xl:text-[80px] md:text-[70px] m992:text-[50px] sc>768:text-[50px]">{t("home.topic.topic5")}</span>
                        </div>
                        <div
                            className={clsx("flex justify-between items-end text", {
                                "animate__animated animate__fadeInRight": isInView,
                            })}
                        >
                        </div>
                    </div>
                    <div className={clsx("2xl:w-[70%] lg:w-[80%] lg:mx-auto sc991:w-full sc991:mt-[24px] 2xl:ml-[117px] xl:ml-[50px] 2xl:mr-[169px] border-t-[2px] border-[#FFFFFF]")}>
                        <div
                            className={clsx(
                                "w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 m992:text-px14 sc<992:mb-[32px] sc991:mb-10 pt-[35px] text-[#FFFFFF] text-justify",
                                { "animate__animated animate__flash": isInView }
                            )}
                            dangerouslySetInnerHTML={{ __html: t("home.description_sub") }}
                        ></div>
                        <div className="sc>768:flex sc>768:justify-center">
                            <Button
                                onClick={showModalContact}
                                color="white"
                                className="sc>768:text-px14 sc<992:mb-[72px] bg-white sc991:mx-auto sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                            >
                                <span className="flex text-[#8C6659] items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18">
                                    {t("button.buy_vouchers")}
                                </span>
                            </Button>{" "}
                        </div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    )

}