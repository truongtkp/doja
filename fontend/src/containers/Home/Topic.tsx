import clsx from "clsx";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import SliderProject from "../../components/Home/SliderProject";
import TopicItem from "../../components/Home/Topic";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import useInView from "../../hooks/useInView";
import TopicProduct from "../../components/Home/TopicProduct";
import { hostBE } from "../../types/host";

export default function Topic() {
  const [t] = useTranslation();
  const navigate = useNavigate();
  const { ref, isInView } = useInView()

  return (
    <div>
      <div className="w-full">
        <TopicItem />
        <div className="w-1920:bg-[length:100%_63%] xl:bg-[length:100%_65%] lg:bg-[length:100%_68%] md:bg-[length:100%_65%] m992:bg-[length:100%_67%] sc991:bg-[length:100%_60%] sm:bg-[length:100%_55%] sm-480:bg-[length:100%_50%] sm-390:bg-[length:100%_50%] lsm-380:bg-[length:100%_47%] lsm-320:bg-[length:100%_48%]"
                  style={{
                    backgroundImage: `url('${hostBE}/fe/young-woman.png')`,
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "bottom",
                  }}>
        <div>
          <div className="lssm:h-auto relative sc<992:h-[690px] lssm:pt-[30px] md:pt-[50px]">
            <TopicProduct />
          </div>
        </div>

        <div className="relative w-1920:h-[100vh] 2xl:h-[100vh] xl:h-[100vh] m992:h-[95vh] md:h-[91vh] sm:h-[58vh] sm-480:h-[45vh] sm-390:h-[43vh] lsm-380:h-[37vh] lsm-320:h-[35vh]">

          <img className="absolute w-1920:top-1/4 2xl:top-1/5 xl:top-[90px] lg:top-48 m992:top-1/4 sc991:top-[40px] sc991:-top-[40px] sm:-top-5 2xl:left-48 xl:left-48 lg:left-24 m992:left-24 sc991:left-24 sm-480:left-8 sm-390:left-8 lsm-380:left-8 lsm-320:left-8 2xl:w-auto xl:w-[18%] lg:w-[20%] m992:w-[18%] sc991:w-[20%]" src={`${hostBE}/fe/Vector-left.png`} />
          <div className="absolute bottom-12 2xl:top-14 xl:top-1/4 m992:top-72 md:top-26 sm:top-20 sc>768:top-64 sm-480:top-8 sm-390:top-16 lsm-360:top-4 lsm-320:top-2 lsm-380:top-8 lsm-320:top-8 right-0 md:right-8 sc>768:right-6 sm:right-8 lsm-320:right-2 2xl:min-w-[50%] w-1920:w-[50%] 2xl:w-[55%] lg:max-w-[55%] md:max-w-[80%] sm:max-w-[80%] m992:max-w-[60%] sc>768:max-w-[90%] sm-480:max-w-[87%] lsm-320:max-w-[98%]">
            <div className="relative">
              <img className="w-full opacity-70" src={`${hostBE}/fe/Vector-right.png`} />
              <div className="absolute w-1920:top-[13rem] 2xl:top-70 xl:top-70 lg:top-24 md:top-40 m992:top-28 sm:top-24 sm-480:top-28 sm-390:top-16 lsm-380:top-28 lsm-360:top-28 lsm-320:top-[90px] 2xl:-right-8 xl:-right-16 lg:-right-32 md:-right-32 m992:-right-36 sm:-right-28 sm-480:-right-20 sm-390:-right-20 lsm-380:-right-16 lsm-360:-right-4 lsm-320:-right-14 w-full xl:w-full">
                <div className="-mt-4 pr-0">
                  <div
                    className={clsx(
                      "flex flex-col w-1920:pl-52 2xl:pl-40 xl:pl-36 w-1920:pt-2 2xl:pt-12 items-end flex-wrap-reverse text-text-title font-bold uppercase 2xl:leading-[35px] md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
                      { "animate__animated animate__fadeInRight": isInView }
                    )}
                  >
                    <span className="Valky 2xl:pb-[28px] sc991:pb-[8px] 2xl:pt-14 lg:pb-2 pb-6 lsm-380:pb-0 text-[#FFFFFF] w-1920:text-[128px] 2xl:text-[100px] xl:text-[60px] m992:text-[45px] sc991:text-[60px] md:text-[48px] lg:text-[48px] sm-480:text-[20px] sm:text-[50px] sm-390:text-[25px] lsm-380:text-[20px] lsm-320:text-[16px]">{t("home.topic.topic4")}</span>
                  </div>
                  <div
                    className={clsx("flex justify-between items-end text", {
                      "animate__animated animate__fadeInRight": isInView,
                    })}
                  >
                  </div>
                </div>
                <div className={clsx("xl:w-[60%] lg:w-[70%] m992:w-[65%] sc991:w-[75%] sc991:w-full sc991:mt-[14px] sm-390:mt-0 lsm-380:mt-0 lsm-320:mt-0 xl:mx-auto")}>
                  <div
                    className={clsx(
                      "sm-480:text-[10px] md:text-[14px] m992:text-px14 sm:text-px14 xl:text-px16 w-1920:text-px20 2xl:text-px18 sc<992:mb-[36px] sc991:mb-4 2xl:pt-[50px] lg:pt-[14px] sm-480:pt-[14px] sm-390:text-[8px] lsm-380:text-[8px] lsm-320:text-[8px] text-[#FFFFFF] text-justify",
                      { "animate__animated animate__flash": isInView }
                    )}
                    dangerouslySetInnerHTML={{ __html: t("home.description_sub") }}
                  ></div>
                  <div className="sc>768:flex sc>768:justify-center">
                    <Button
                      onClick={() => navigate("/lien-he")}
                      color="primary"
                      className="sc>768:text-px14 text-px16 sc<992:mb-[72px] bg-white sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
                    >
                      <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 lsm-320:text-[12px]">
                        {t("button.booking")}
                      </span>
                    </Button>{" "}
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          {/* <h2 className="text-[#11213F]  sc991:text-text-primary lssm:text-px20 md:text-[32px]  xl:text-[34px] 2xl:text-[48px]  sc991:text-center font-bold   mb-[40px] pl-[20px] border-l-[25px] border-solid uppercase border-primary sc991:border-none block leading-[35px]">
            {t("home.topic.topic3")}
          </h2>
          <SliderProject /> */}
        </div>
        </div>
      </div>
    </div>
  );
}
