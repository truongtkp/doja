
import { useTranslation } from "react-i18next";
import ManagerSteffItem from "../../../components/managerModal/StaffSlideStore";
import {  useAppSelector } from "../../../hooks/hook";



export default function StaffList () {
    const steff = useAppSelector(state => state.steff)
    const [t] = useTranslation();

    return (
        steff.listStaff.length > 0 ? (
        <div className="lssm:my-[38px] md:my-[80px]">
            <h3 className="text-px20 mb-[38px] text-text-primary font-medium sc>768:hidden">{t("dashboard.recruitment.list_staff")}</h3>
            <div>
                {
                    steff.listStaff.map((value, index) => {
                        return <ManagerSteffItem steff={value} key={index} />
                    })
                }
            </div>

          
        </div>

        ): <div className="h-[400px] w-full flex justify-center items-center">{t("common.no_data")}</div>
    )
}