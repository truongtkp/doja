import { useTranslation } from "react-i18next"
import { FormikValues,useFormik } from "formik";
import * as Yup from "yup";
import {loadScript} from "@paypal/paypal-js"
import { useEffect, useState} from "react";

export default function PopUpForm({data,setter}:any) {
    const [orderId,setOrderId] = useState("");
    const [cacheValue , setCacheValue] = useState({
        fullname: "",
        phone: "",
        email: "",
        content: ""
    });

    const [t] = useTranslation();
    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    const clientId = "AUIedOs8C2i7GhroqNCuxF1Rkay0KlcfdyJ876DSoEpPQ84w-wE7U9M-oEOa3xWELhPp5E_TbMQUDfmK";
    const formik = useFormik({
        initialValues: {
            fullname: '',
            phone: "",
            email: "",
            content: "",
            callback: (values:FormikValues) => {
                // Override interfece here
            }
        },
        validationSchema: Yup.object({
            fullname: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
            email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
            notcontente: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
        }),
        onSubmit: (values) => {
            console.log("Validation Successful!");
            let temp = cacheValue;
            temp.fullname = values.fullname;
            temp.phone = values.phone;
            temp.email = values.email;
            temp.content = values.content;
            setCacheValue(temp);
            values.callback(values);
        }
    })


    const generateSdkScript = (token:any) => {
        loadScript({
          "sdkBaseURL":"https://www.paypal.com/sdk/js",
          components: "buttons,hosted-fields",
          "client-id":clientId,
          "data-client-token": token.client_token
        
        }).then((paypal) => {
    
          if (!paypal?.HostedFields?.isEligible()){
            console.log("NOT ELIGABLE");
            return;
          }
          
          console.log("HOSTED FIELDS IS ELIGABLE");
          paypal.HostedFields.render(
            {
              createOrder: () => {
                console.log(cacheValue);
                const temp:string[] = data.time.split(':');
                const hour:number = parseInt(temp[0]);
                const minute:number = parseInt(temp[1]);

                return fetch("/api/order-voucher", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        addressId: parseInt(data.location.id),
                        email: cacheValue.email,
                        fullName: cacheValue.fullname,
                        phoneNo: cacheValue.phone,
                        packageId: parseInt(data.service.id),
                        staffId: parseInt(data.staff.id),
                        hour: hour,
                        minute: minute,
                        bookingDate: dateToLocalTimeISOString(data.bookingDate).split('T')[0]
                    })
                }).then((res) => {
                  return res.json()
                }).then((orderData) => {
                  console.log(orderData);
                    setOrderId(orderData.id);
                  // orderId = orderData.id; // needed later to complete capture
                  return orderData.id;
                }).catch((error) => {
                  console.log(error);
                });
              },
              fields: {
                number: {
                  selector: "#card-number",
                  placeholder: "1111 1111 1111 1111"
                  // value: "test"
                },
                cvv: {
                  selector: "#cvv",
                  placeholder: "123"
                  
                },
                expirationDate: {
                  selector: "#expiration-date",
                  placeholder: "00/0000"
                }
              }
            }
          ).then((cardField) => {

            document?.querySelector("#card-form")?.addEventListener("submit", (event) => {
              event.preventDefault();
              formik.submitForm(); // update value
            })


            formik.values.callback = (values:FormikValues) => {
                cardField.submit().then((res) => {

                    console.log("tempo");
                });
            }
          })
            
          // console.log("The order id is: " + orderId);
        }).then((res) => {
            data.back = true;
            setter(data);
        })
        .catch((err) => {
          console.log("Failed to generate paypal SDK",err);
        })
    };

    useEffect(() => {

        data.callback = () => {
            // ASYN FUNCTION WRAPPER
            formik.submitForm();
            return false;
        }
        data.back = false;
        setter(data);
        fetch ("/api/paypal/token-identity", {method: "POST"}).then(
            (res) => {return res.json()}
        ).then ((token) => {
            generateSdkScript(token);
        })
    },[]);


    const resetForm = () => {
        formik.resetForm();
    }



    return (
        <div className="relative bg-[#F9F3EF] lssm:p-[18px] sc991:mx-1 xl:p-[30px] lg:p-[35px] xl:pb-20 lg:pb-34 w-max lssm:mt-[30px] lg:mt-0 border w-[98%] bg-white text-darkBlue  shadow rounded-[10px]">
                <img src="/images/GitDecor.png" className="absolute 2xl:top-36 lg:top-28 2xl:right-36 m992:right-16 xl:top-28 m992:top-28 sc991:top-28 w-[15%] xl:right-16 lg:right-16 w-1920:top-32 sc991:right-14 w-1920:right-36 xl:top-18 right-0" alt="PopUp Image" />
                <img src="/images/GitDecor2.png" className="absolute 2xl:bottom-8  xl:w-[80%] lg:w-[70%] bottom-6 m992:w-[65%] sc991:w-[75%] lg:right-32 m992:right-24" alt="PopUp Image" />
                <div className="w-1920:w-[1000px] 2xl:w-[850px] xl:w-[850px] m-auto">
                <div className="">
                    <img src="/images/Gift.png" className="xl:w-[30%] lg:w-[30%] m992:w-[35%] sc991:w-[35%]" alt="logo" />
                </div>
                <div className="w-1920:w-[700px] 2xl:w-[650px] xl:w-[600px] m-auto">
                <form onSubmit={formik.handleSubmit} className="lssm:p-[28px] lg:p-18 xl:px-4 flex flex-col justify-center items-center">
                    <div className="mt-8 w-full z-10">
                        <div className="mt-[16px] mb-3 flex justify-start items-center">
                            <h2 className="text-[#BF8782] w-1920:text-px20 2xl:text-px20 xl:text-px16 font-semibold mr-6"> {t("button.amount")} </h2>
                            <p className="text-[#BF8782] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold"> {data.service.price} USD$</p>
                        </div>
                        <div className="mt-[20px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 2xl:text-px20 xl:text-px16 sc991:text-px14 font-semibold text-[#68483C]">Email</label>
                            <input
                                type="text"
                                name="fullname"
                                placeholder=""
                                // value={formik.values.fullname}
                                // onChange={formik.handleChange}
                                className="w-full h-[40px] bg-transparent border-b border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.fullname}
                            </span>
                        </div>

                        <div className="mt-[16px] mb-5 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Phone</label>
                            <input
                                type="text"
                                name="phone"
                                placeholder=""
                                // value={formik.values.phone}
                                // onChange={formik.handleChange}
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.phone}
                            </span>
                        </div>

                        <div className="mt-[24px] mb-[24px] flex justify-start items-center">
                            <h2 className="text-[#68483C] w-1920:text-px20 2xl:text-px20 m992:text-px16 xl:text-px16 font-semibold"> {t("button.title")} </h2>
                        </div>

                        {/* <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Name card</label>
                            <input
                                type="email"
                                name="email"
                                placeholder=""
                                // value={formik.values.email}
                                // onChange={formik.handleChange}
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.email}
                            </span>
                        </div> */}

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Card number</label>
                            <div
                                id = "card-number"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.email}
                            </span>
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Expiration date</label>
                            <div
                                id = "expiration-date"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.email}
                            </span>
                        </div>

                        <div className="mt-[16px] mb-3 flex justify-between items-center">
                            <label className="form-label w-1920:w-[30%] 2xl:w-[35%] xl:w-[30%] lg:w-[45%] m992:w-[40%] sc991:w-[45%] w-1920:text-px20 m992:text-px16 2xl:text-px20 sc991:text-px14 xl:text-px16 font-semibold text-[#68483C]">Security code</label>
                            <div
                                id= "cvv"
                                placeholder=""
                                className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                            />
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.content}
                            </span>
                        </div>

                        
                        <div className="mt-[24px] mb-3 flex items-center">
                            <input className="relative float-left mt-[0.15rem] border-[#D9A283] mr-[6px] h-[1.125rem] w-[1.125rem] appearance-none rounded-[0.25rem] border-[0.125rem] border-solid border-neutral-300 outline-none before:pointer-events-none before:absolute before:h-[0.875rem] before:w-[0.875rem] before:scale-0 before:rounded-full before:bg-transparent before:opacity-0 before:shadow-[0px_0px_0px_13px_transparent] before:content-[''] checked:border-primary checked:bg-primary checked:before:opacity-[0.16] checked:after:absolute checked:after:ml-[0.25rem] checked:after:-mt-px checked:after:block checked:after:h-[0.8125rem] checked:after:w-[0.375rem] checked:after:rotate-45 checked:after:border-[0.125rem] checked:after:border-t-0 checked:after:border-l-0 checked:after:border-solid checked:after:border-white checked:after:bg-transparent checked:after:content-[''] hover:cursor-pointer hover:before:opacity-[0.04] hover:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:shadow-none focus:transition-[border-color_0.2s] focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-[0.875rem] focus:after:w-[0.875rem] focus:after:rounded-[0.125rem] focus:after:content-[''] checked:focus:before:scale-100 checked:focus:before:shadow-[0px_0px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] checked:focus:after:ml-[0.25rem] checked:focus:after:-mt-px checked:focus:after:h-[0.8125rem] checked:focus:after:w-[0.375rem] checked:focus:after:rotate-45 checked:focus:after:rounded-none checked:focus:after:border-[0.125rem] checked:focus:after:border-t-0 checked:focus:after:border-l-0 checked:focus:after:border-solid checked:focus:after:border-white checked:focus:after:bg-transparent dark:border-neutral-600 dark:checked:border-[#D9A283] dark:checked:bg-[#D9A283]" type="checkbox" value=""/>
                            <label className="ml-2 text-px16 sc991:text-px14 text-[#68483C] font-semibold">Tôi đồng ý các điều khoản của nhà cung cấp dịch vụ*</label>
                        </div>
                    </div>
                </form>
                    <div className="text-end relative">
                        <button 
                        className="btn-popup shadow-md w-[134px] h-[24px] sc991:w-[110px] sc991:h-[28px] rounded lg:w-[138px] lg:h-[40px] lg:rounded-[10px] border-[1px] border-[#000000] text-[12px] lg:text-[18px] font-bold text-[#65493A] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px]" type="submit">
                            {t("button.cancel")}
                        </button>
                        <button 
                        className="btn-popup shadow-md w-[134px] h-[24px] sc991:w-[110px] sc991:h-[28px] rounded lg:w-[138px] lg:h-[40px] lg:rounded-[10px] text-[12px] lg:text-[18px] font-bold text-white bg-[#68483C] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px] ml-4" type="submit">
                            {t("button.pay")}
                        </button>
                    </div>
                </div>
                </div>
            </div>
    )
}