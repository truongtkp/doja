import { useTranslation } from "react-i18next"
import { useFormik } from "formik";
import { useAppDispatch } from "../../hooks/hook";
import Button from "../../components/Button";
import * as Yup from "yup"
import PopUpForm2  from "../Contact/PopupForm2";
import { showModal } from "../../reducers/modal";
import { useState, useEffect } from "react";
import Calendar from "react-calendar";
import AmountManager from "../../services/amount";

export default function PopUpForm() {
    const [overlay, setOverlay] = useState(false);
    const [priceList, setPriceList] = useState([]);
    const [date, setDate] = useState((() => {
        const tmpDate = new Date();
        tmpDate.setDate(tmpDate.getDate() + 1);
        tmpDate.setHours(0,0,0,0);
        return tmpDate}
    )());

    const [finalData,setFinalData] = useState({
        to: "",
        from: "",
        phone: "",
        email: "",
        content: "",
        service: {
            id: 0,
            price: ""
        }
    });

    const [t] = useTranslation();
    const dispatch = useAppDispatch();

    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    const handleDateChange = (param:Date) => {
        setOverlay(false);
        if (new Date().getTime() >= param.getTime()){
            return;
        }
        setDate(param);

    }

    const formik = useFormik({
        initialValues: {
            to: "",
            from: ""

            // fullname: finalData.fullname,
            // phone: finalData.phone,
            // email: finalData.email,
            // content: finalData.content,
        },
        validationSchema: Yup.object({
            to: Yup.string().required(t("validate.error.required")),
            from: Yup.string().required(t("validate.error.required")),
            // fullname: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.name"), min: 5 })),
            // phone: Yup.string().required(t("validate.error.required")).matches(/^[0-9]{10}$/, t("validate.error.pattern", { name: t("contact.form.phone") })),
            // email: Yup.string().required(t("validate.error.required")).matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, t("validate.error.pattern", { name: t("career.form.email") })),
            // notcontente: Yup.string().required(t("validate.error.required")).min(5, t("validate.error.min", { name: t("contact.form.help"), min: 5 })),
        }),
        onSubmit: (values) => {
            let temp = finalData;
            temp.to = values.to;
            temp.from = values.from;
            // temp.email = values.email;
            // temp.content = values.content;
            console.log("temporary");
            setFinalData(temp);

            // temp.phone = 
            dispatch(showModal(<PopUpForm2 data={finalData} setter={setFinalData}/>))

            // contactService.postContact(values).then(() => {
            //     dispatch(pushPopup({
            //         type: "SUCCESS",
            //         message: t("contact.form.postSuccess")
            //     }))

            //     resetForm()
            // }).catch(() => {
            //     dispatch(pushPopup({
            //         type: "ERROR",
            //         message: t("contact.form.postFail")
            //     }))
            // })
        }
    })

    useEffect(() => {
        console.log("START POPUP 1");
        AmountManager.get({page: 0,limit: 100}).then((res) => {
            console.log(res.total); 
            let tmp = finalData;
            tmp.service.id = res.list[0].id;
            tmp.service.price = res.list[0].price;
            setFinalData(tmp)
            setPriceList((res.list as any));
        })
    },[]); 

    const parsePrice = () => {
        const rows = [];
        for (let i = 0; i < priceList.length; i++){
            const id = (priceList[i] as any).id;
            const price = (priceList[i] as any).price;
            rows.push(
                <option value={id} key={i}>{price}</option>
            );
            // tmp.service.id = priceList[i].
        }
        return rows;
    }

    const handleSelectedPrice = (event:any) => {
        const id = event.target.value;
        const price = event.target.options[event.target.selectedIndex].text;
        let tmp = finalData;
        tmp.service.id = id;
        tmp.service.price = price;
        setFinalData(tmp);
    }

    // const resetForm = () => {
    //     formik.resetForm();
    // }

    const showModalContact = () => {

        formik.submitForm();
    };

    return (
        <div className="relative bg-[#F9F3EF] lssm:p-[24px] sc991:mx-1 lg:p-[54px] lg:pb-60 w-max lssm:mt-[30px] lg:mt-0 border w-[98%] bg-white text-darkBlue  shadow rounded-[10px]">
            <img src="/images/GitDecor.png" className="absolute 2xl:top-36 lg:top-28 2xl:right-30 m992:w-[15%] xl:top-36 w-[15%] xl:right-16 lg:right-12 m992:right-16 w-1920:top-32 sc991:right-14 sc991:top-14 w-1920:right-20 xl:top-18 right-0" alt="PopUp Image" />
            <img src="/images/GitDecor2.png" className="absolute 2xl:bottom-8 xl:w-[80%] m992:w-[80%] sc991:w-[75%] bottom-8 lg:right-16" alt="PopUp Image" />
            <div className="w-1920:w-[850px] 2xl:w-[850px] xl:w-[850px]">
            <div className="">
                <img src="/images/Gift.png" className="xl:w-[30%] m992:w-[30%] sc991:w-[28%]" alt="logo" />
            </div>
            <div className="w-1920:w-[600px] 2xl:w-[600px] xl:w-[550px] m-auto">
            <form onSubmit={formik.handleSubmit} className="lssm:p-[28px] lg:p-[35px] flex flex-col justify-center items-center">
                <div className="mt-8 w-full z-10">
                    <div className="mb-3 flex justify-between items-center">
                        <label className="form-label w-[30%] xl:w-[20%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">To</label>
                        <input
                            type="text"
                            name="to"
                            placeholder=""
                            value={formik.values.to}
                            onChange={formik.handleChange}
                            className="w-full h-[40px] bg-transparent border-b border-complementary-000 outline-none"
                        />
                        <div>
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.to}
                            </span>
                        </div>
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[30%] xl:w-[20%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">From</label>
                        <input
                            type="text"
                            name="from"
                            placeholder=""
                            value={formik.values.from}
                            onChange={formik.handleChange}
                            className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                        />
                        <div>
                            <span className="text-[14px] text-red-600 mt-1">
                                {formik.errors.from}
                            </span>
                        </div>
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[30%] xl:w-[20%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16 font-semibold text-[#68483C]">Amount</label>
                        <select
                            name="amount"
                            placeholder=""
                            // value={formik.values.email}
                            onChange={(val) => handleSelectedPrice(val)}
                            className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none"
                        >
                            {parsePrice()}
                        </select>
                        <div>
                            <span className="text-[14px] text-red-600 mt-1">
                                {/* {formik.errors.email} */}
                            </span>
                        </div>
                    </div>

                    <div className="mt-[16px] mb-3 flex justify-between items-center">
                        <label className="form-label w-[30%] xl:w-[20%] w-1920:text-px20 2xl:text-px20 xl:text-px16 m992:text-px16  font-semibold text-[#68483C]">Expires</label>

                        <a
                            href="#" onClick={(e) => {
                                e.preventDefault();
                                setOverlay(!overlay)
                            }}
                            className="w-full h-[40px] border-b bg-transparent border-complementary-000 outline-none relative"
                        >
                            {date.toDateString()}
                        </a>
                        <span className="text-[14px] text-red-600 mt-1">
                            {/* {formik.errors.content} */}
                        </span>
                    </div>
                    <div className="relative w-[100%]">
                    {overlay && (
                        <div className="w-[20rem] absolute top-0 left-0 items-center bg-white border-2 border-t-border-box">
                            <Calendar
                            value={date} onChange={(e:any) => handleDateChange(e)}
                            />
                        </div>
                    )}
                    </div>
                </div>
            </form>
                <div className="mt-[16px] text-end relative">
                    <Button 
                    onClick={showModalContact}
                    color="white"
                    className="inline-block btn-popup shadow-md w-[134px] h-[24px] sc991:w-[110px] sc991:h-[36px] m992:h-[32px] rounded lg:w-[138px] lg:h-[40px] lg:rounded-[10px] text-[12px] lg:text-[18px] font-bold text-white bg-[#A68276] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px]" type="submit">
                        {t("button.continue")}
                    </Button>
                </div>
            </div>
            </div>
        </div>
    )
}