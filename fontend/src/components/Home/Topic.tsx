import clsx from "clsx";
import { useNavigate } from "react-router-dom";
import useInView from "../../hooks/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import { useTranslation } from "react-i18next";
import { useAppSelector } from "../../hooks/hook";
import { hostBE } from "../../types/host";

export default function TopicItem() {
  const { ref, isInView } = useInView();
  const [t] = useTranslation();
  const navigate = useNavigate();
  const translate = useAppSelector((state) => state.translateSlice);
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div
      className="lssm:pt-[30px] md:pt-[100px] lssm:px-[24px] md:px-[80px]  xl:px-[50px]  w-1920:px-[162px] relative"
      style={{
        backgroundImage: `url('${hostBE}/fe/bold-top.png')`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "contain",
      }}
      ref={ref}
    >
      <img className="absolute -bottom-20 left-0 " src={`${hostBE}/fe/line-left.png`} />
      <img className="absolute 2xl:top-14 xl:top-26 w-1920:top-28 xl:top-18 lg:top-8 m992:top-8 md:top-8 sm:top-14 right-0" src={`${hostBE}/fe/line-right.png`} />
      <div className="-mt-4 pr-0">
        <div
          className={clsx(
            "flex items-end  text-text-title font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] xl:text-[34px] 2xl:text-[48px]",
            { "animate__animated animate__fadeInRight": isInView }
          )}
        >
          <img src={`${hostBE}/fe/icon-topic1.png`} className="xl:w-[151px] lg:w-[90px] w-[70px]" alt="icon-topic1.png"/>
          <span className="xl:pb-12 Valky lg:pb-12 pb-6 text-[#85512B]">{t("home.topic.topic1")}</span>
        </div>
        <div
          className={clsx("flex justify-between items-end text", {
            "animate__animated animate__fadeInRight": isInView,
          })}
        >
        </div>
      </div>

      <div className="relative">
        <div
          className={clsx(
            " sc<992:flex justify-between sc991:flex-col  xl:px-[151px]"
          )}
        >
           <div className={clsx("w-[55%] sc<992:mr-[60px] sc991:w-full sc991:mt-[24px]")}>
            <div
              className={clsx(
                "lssm:text-px14 w-1920:text-px20 2xl:text-px18 xl:text-px16 md:text-px16 sc<992:mb-[32px] sc991:mb-3 text-[#64483B] text-justify",
                { "animate__animated animate__flash": isInView }
              )}
              dangerouslySetInnerHTML={{ __html: t("home.description_sub") }}
            ></div>
            <div className="sc>768:flex sc>768:justify-center">
              <Button
                onClick={() => navigate("/gioi-thieu")}
                color="primary"
                className="sc>768:text-px14 text-px16 sc<992:mb-[72px] sc991:mx-auto sc991:mb-6 w-1920:h-[60px] 2xl:h-[55px] xl:h-[50px] lg:h-[45px] m992:h-[45px] md:h-[45px] sm:h-[40px] sm-480:h-[40px] rounded-[5px] w-1920:w-[150px] 2xl:w-[140px] xl:w-[130px] lg:w-[130px] m992:w-[130px] md:w-[130px] sm:w-[120px] sm-480:w-[120px] shadow-md"
              >
                <span className="flex items-center text-inherit font-medium w-1920:text-px20 2xl:text-px18 m992:text-px16 md:text-px16">
                  {t("button.see_more")}
                </span>
              </Button>{" "}
            </div>
            <div></div>
          </div>
          <div
            ref={refFrame}
            className={clsx(
              "sc<992:w-[50%] h-full sc991:w-full sc991:order-1 relative",
              {
                "animate__animated animate__fadeInRight": isInView,
              }
            )}
          >
            <iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" scrolling="no" allowFullScreen={true}  className='sc991:w-full lssm:h-[280px]  md:h-[370px] lg:h-[540px] xl:h-[332px] md:w-full object-cover rounded-[10px]'  width="100%" height="332" src={`https://www.youtube.com/embed/${translate.isEnglish ? "wKO9z2t9CVw" : "aUatyOjy3A8"}`}
             title="Anfico" ></iframe>
          
          </div>

         
        </div>
      </div>
    </div>
  );
}
