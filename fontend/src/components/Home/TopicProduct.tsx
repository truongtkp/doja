import clsx from "clsx";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { CgArrowLongRight } from "react-icons/cg";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import useInView from "../../hooks/useInView";
import { changeCategoryChecked } from "../../reducers/categorySlice";
import { getProductActiveHome } from "../../reducers/productPublic";
import { hostBE } from "../../types/host";

import Button from "../Button";

export default function TopicProduct() {
  const { ref, isInView } = useInView();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const translate = useAppSelector((state) => state.translateSlice);
  const categories = useAppSelector(
    (state) => state.productPublic.productActiveHome
  );
  const [t] = useTranslation();

  const handleNavigate = (id: number) => {
    navigate("/sanpham");
    dispatch(changeCategoryChecked(id));
  };
  useEffect(() => {
    // if (productActiveHome.length === 0) {
    dispatch(getProductActiveHome());
    // }
  }, []);

  return (
    <div
      className="w-1920:pb-28 2xl:pb-56 lssm:pb-32 lssm:pt-[30px] 2xl:pt-[30px] h-full lssm:px-[24px] lg:px-28 md:px-10 xl:px-[50px] w-1920:px-[162px] "
      ref={ref}
      style={{
        backgroundImage: `url('${hostBE}/fe/bg-service.png')`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "100% 100%",
      }}
    >
      <div
        className={clsx(
          "flex items-end text-white font-bold uppercase 2xl:leading-[35px] lssm:text-px20 md:text-[32px] m992:mt-4 sc991:mt-5 xl:text-[34px] 2xl:text-[48px]",
          { "animate__animated animate__fadeInRight": isInView }
        )}
      >
        <img src={`${hostBE}/fe/icon-topic2.png`} className="xl:w-[151px] lg:w-[90px] md:w-[80px] w-[70px]" alt="icon-topic1.png"/>
        <span className="xl:pb-8 lg:pb-8 pb-6 Valky">{t("home.topic.topic2")}</span>
      </div>
      <div className="grid grid-cols-1 lg:grid-cols-2 2xl:gap-8 xl:gap-14 lg:gap-4 w-full h-full w-1920:px-20 2xl:px-[8rem] lg:px-[30px] xl:px-[80px] lssm:px-[10px]">
        <div
          className={clsx(
            "",
            { "animate__animated animate__fadeInLeft": isInView }
          )}
        >
          <img src={`${hostBE}/fe/image_project.png`}  className="w-full w-1920:w-[38rem] 2xl:w-[32rem] xl:w-[24rem] lg:w-[19rem] m992:w-[20rem] sc>768:w-[24rem] md:w-[24rem] mx-auto p-8" alt="" />
        </div>
        <div className="">
          {/* <h4 className="font-semibold lssm:text-px14 md:text-px16 text-center lg:text-[32px] text-white-color lssm:my-[24px] lg:mb-[47px]">
            {t("home.supplier")}
          </h4> */}
          <div className="w-1920:pt-24 grid lssm:grid-cols-2 sm-480:grid-cols-3 gap-x-[24px] gap-y-[26px] xl:mt-16">
            {categories.map((item) => {
              return (
                <div className="text-center" key={item.id} onClick={() => handleNavigate(item.id)}>
                  <div
                    className={clsx(
                      "mx-auto w-1920:w-[133px] 2xl:w-[110px] xl:w-[90px] lg:w-[85px] m992:w-[95px] sc991:w-[100px] max-w-full w-1920:h-[133px] 2xl:h-[110px] xl:h-[90px] lg:h-[85px] m992:h-[95px] sc991:h-[100px] m992:mt-[20px] sc991:mt-[15px] rounded-[50%] border-2 border-solid border-white-color flex items-center justify-center cursor-pointer",
                      { "animate__animated animate__fadeInDown": isInView }
                    )}
                  >
                    <div className="w-1920:w-[90px] 2xl:w-[80px] xl:w-[70px] lg:w-[65px] m992:w-[55px] sc991:w-[60px] w-1920:h-[90px] 2xl:h-[80px] xl:h-[70px] lg:h-[65px] m992:h-[55px] sc991:h-[60px]">
                      <img className="w-full" src={item.imageUrl} alt="" />
                    </div>
                  </div>
                  <p
                    className={clsx(
                      "text-px14 2xl:text-px20 xl:text-px18 m992:text-px18 sc991:text-px18 line-clamp-2 text-white-color mt-[14px] cursor-pointer",
                      { "animate__animated animate__flash": isInView }
                    )}
                  >
                    {item.nameEn}
                  </p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
