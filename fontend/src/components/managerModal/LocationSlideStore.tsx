import clsx from "clsx";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { pushPopup } from "../../reducers/popupSlice";
import { LableContent } from "./LableContent";
import LocationManager from "../../services/location";
import { editLocation } from "../../reducers/locationmanagerSlice";

type categoryType = {
  createdBy: string;
  createdDate: string;
  id: number;
  name: string;
  activated: boolean;
}[];

type LocationsProps = {
  handleCurrenPage?: () => void;
};

function LocationSlideStore({ handleCurrenPage }: LocationsProps) {
  const dispatch = useAppDispatch();
  const navigator = useNavigate();
  const param = useParams();
  const navigationPrevRef = useRef(null);
  const navigationNextRef = useRef(null);
  const scroToTop = useRef<HTMLDivElement>(null);
  const [files, setFiles] = useState<any[] | []>([]);
  const [preview, setPreview] = useState<any[]>([]);
  const [contentvi, setContentVi] = useState<any>("");
  const [contentEn, setContentEn] = useState<any>("");
  const [isdisAble, setDisable] = useState<any>("");
  const refLocation =  useRef<any>();
  const { isEnglish } = useAppSelector((state) => state.translateSlice);

  const [valueForm, setValueForm] = useState({
    name: "",
    address: "",
    description: "",
  })

  const handleChangeValueForm = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=> {
    const name = event.target.name;
    const value = event.target.value;

    setValueForm({
      ...valueForm,
      [name]: value
    })
  }

  const refNewVi = useRef<any>();
  const refNewEn = useRef<any>();
  const listImageFiles = useRef<
    {
      id: number;
      file: File | string;
    }[]
  >([]);
  const listImageFilesEn = useRef<
    {
      id: number;
      file: File | string;
    }[]
  >([]);

  const hiddenformAdd = () => {
    if (handleCurrenPage) {
      handleCurrenPage();
    }
  };

  const handleSubmit = async () => {
    const formLocationSend: any = { ...valueForm };

    const output = await refLocation.current?.save();
    if(
      valueForm.name == "" ||
      valueForm.address == "" ||
      valueForm.description == ""
    ) {
      dispatch(
        pushPopup({
          message: isEnglish
            ? "Chưa nhập đầy đủ thông tin các trường"
            : "Full information not enter",
          type: "WARNING",
        })
      );
    } else {
      const formLocationSend: any = { ...valueForm };
      const content = JSON.stringify(output);
      setContentVi(content);

      if (param.id) {
          formLocationSend.id = param.id;
          formLocationSend.contentEn = contentEn;
          dispatch(editLocation(formLocationSend));
          navigator("/quanly/diadiem");
    } else {

      formLocationSend.contentEn = contentEn;
      const result = await LocationManager.postLocation(formLocationSend);
          if (result) {
            dispatch(
              pushPopup({
                type: "SUCCESS",
                message: isEnglish
                  ? "Thêm địa chỉ thành công."
                  : "Add successful location.",
              })
            );
            navigator("/quanly/diadiem");
          } else {
            dispatch(
              pushPopup({
                type: "WARNING",
                message: isEnglish
                  ? "Không thêm địa chỉ được."
                  : "Can't add location.",
              })
            );
            setDisable(true);
          }
    }
      };
    };

  useEffect(() => {
      if (param.id) {
        const idedit = parseInt(param.id);
        const callDetail = async () => {
          const editLocationDetail = await LocationManager.getById(idedit);
          setValueForm({
            ...valueForm,
            ...editLocationDetail,
          });
        };
        callDetail();
      }
    }, []);

  return (
    <div
      ref={scroToTop}
      className="w-full w-1920:my-[120px] mt-0  h-auto bg-white"
    >
          <div className="w-full flex justify-end h-auto m992:pl-7 sm-480:pl-6">
            <div className="w-full h-auto bg-white rounded-[20px] sm-480:pb-[70px] w-1920:px-0 md:px-8 pl-1 sm-480:mr-2 mr-[4px]">
              <p className="py-[50px] sm-480:text-px32 font-bold text-xl lg:text-black text-primary w-full text-center">
                {param.id ? "EDIT LOCATION DETAIL" : "ADD NEW LOCATION"}
              </p>

              {/* tên sản phẩm input */}
              <div className="w-full sm-480:mb-12 mb-[30px]">
                <LableContent content="Name Store" />
                <input
                  value={valueForm.name}
                  name="name"
                  onChange={(event) => {
                   handleChangeValueForm(event);
                  }}
                  type="text"
                  className="w-full px-5 py-3 sm-480:text-base text-sm focus:outline-none border border-border-gray rounded-md"
                />
              </div>

                {/* Địa chỉ quán input */}
                <div className="w-full sm-480:mb-12 mb-[30px]">
                <LableContent content="Location Store" />
                <input
                  value={valueForm.address}
                  name="address"
                  onChange={(event) => {
                   handleChangeValueForm(event);
                  }}
                  type="text"
                  className="w-full px-5 py-3 sm-480:text-base text-sm focus:outline-none border border-border-gray rounded-md"
                />
              </div>

              {/* mô tả sản phẩm input */}

              <div className="w-full sm-480:mb-12 mb-[30px]">
                <LableContent content="Location Description" />
                <textarea
                  rows={10}
                  cols={100}
                  value={valueForm.description}
                  name="description"
                  onChange={(event) => {
                   handleChangeValueForm(event);
                  }}
                  className="p-3 focus:outline-text-primary sm-480:text-base text-sm rounded-[10px] border border-border-gray w-full"
                ></textarea>
              </div>

              <div className="text-right mb-[60px]">
                <Link
                  to="/quanly/diadiem"
                  className="inline-block 2xl:py-[18px] py-[14px] mr-3 2xl:w-[180px] sm-390:w-[150px] w-[100px] border rounded-md border-text-lightred text-text-lightred font-medium sm-480:text-base text-sm text-center"
                >
                  Go back
                </Link>
                <button
                  disabled={isdisAble}
                  onClick={handleSubmit}
                  className={clsx(
                    isdisAble && "cursor-not-allowed",
                    "2xl:py-[18px] cursor-pointer py-[14px] inline-block font-medium 2xl:w-[180px] sm-390:w-[150px] w-[100px] border rounded-md bg-primary text-white sm-480:text-base text-sm text-center"
                  )}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
    </div>
  );
}

export default LocationSlideStore;