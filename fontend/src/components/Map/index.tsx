type Props = {
  width: number | string;
  height: number | string;
};

export default function Map({ width, height }: Props) {
  return (
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.314130018998!2d105.8536282153851!3d21.020113293448194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abed0c8bc00f%3A0x98f46133a40c9576!2zMjhBIFRy4bqnbiBIxrBuZyDEkOG6oW8sIFBoYW4gQ2h1IFRyaW5oLCBIb8OgbiBLaeG6v20sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1676523668612!5m2!1svi!2s"
      width={width}
      height={height}
    ></iframe>
  );
}
