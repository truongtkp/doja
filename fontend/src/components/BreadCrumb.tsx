import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { hostBE } from "../types/host";

type breamcrumType = {
  name: string;
  path: string;
  icon?: boolean;
};
function BreadCrumb() {
  const { t, i18n } = useTranslation();
  const breadCrumbList: breamcrumType[] = [
    {
      name: "header.menu.home",
      path: "/",
      icon: true,
    },
    {
      name: "header.menu.introduce",
      path: "/bsanpham",
    },
    {
      name: "header.menu.home",
      path: "/c",
    },
  ];
  return (
    <div className="py-9 w-1920:px-[162px] md:px-[100px] sm-480:px-[44px] lssm:px-3">
      <ul className="w-full h-auto">
        {breadCrumbList.map((item, index) => {
          return (
            <li key={index} className="inline-block mr-8">
              <Link
                to={item.path}
                className={
                  index + 1 === breadCrumbList.length
                    ? "relative"
                    : "breamItem relative"
                }
              >
                {item.icon && (
                  <img
                    src={`${hostBE}/fe/VectorHomeicon.png`}
                    alt="home"
                    className=" w-[18px] h-5 inline-block align-top  mr-3"
                  />
                )}
                <span
                  className={
                    item.icon
                      ? "inline-block text-sm text-[#1A73E8]"
                      : "inline-block text-sm"
                  }
                >
                  {t(`${item.name}`)}
                </span>
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default BreadCrumb;
