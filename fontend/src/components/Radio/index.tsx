import { ChangeEvent } from "react"


type Props = {
    id?: string
    name?: string
    value?: string
    onChange?: (event:ChangeEvent<HTMLInputElement>)=> void ,
    checked?: boolean
    color?: string
}


export default function Radio({id, name,value, onChange, checked, color}:Props) {

    return <>
        <input hidden type="radio" className="radio-input" defaultChecked={checked ? checked : false} id={id} name={name} onChange={onChange} value={value} />
      
            <label htmlFor={id} className={`radio-label `} style={{["--color" as any]: color ? color : "#DADADA"}} >
        </label>
    </> 
}