import React from "react";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

export default function Final({data,setter}:any) {
    const [isBusy, setIsBusy] = useState(true);

    const [t] = useTranslation();
    useEffect(() => {
        let temp = data;
        // GET ADDRESS BY ID
        (async function() {

            await Promise.all([
                fetch ("/api/address/" + temp.location.id).then((res) => {
                    return res.json();
                }).then((res) => {
                    temp.location.name = res.name;
                    temp.location.address = res.address;
                }),


                fetch ("/api/package/" + temp.service.id).then((res) => {
                    return res.json();
                }).then((res) => {
                    console.log(res);
                    temp.service.title = res.title;
                    temp.service.price = res.price;
                }),

                fetch("/api/staff/"+temp.staff.id).then((res) => {
                    return res.json();
                }).then((res) => {
                    temp.staff.name = res.name;
                })
            ]);

            console.log("tefadsf");
            setIsBusy(false);
        }) ();


        temp.callback = () => {
            return true;
        }
        setter(temp);
    },[]);

    return (
    <div className="w-full rounded-[10px]">
        {!isBusy && (<></>)}
        <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
        <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description")}</p>
        
         {/* Title start */}
        <div className="text-center bg-[#BC8A79] py-2.5 rounded-t-[25px]">
            <h2 className="text-px32 font-bold leading-10 text-white">LỊCH HẸN CỦA BẠN</h2>
        </div>
        {/* Title end */}

        {/* Content start */}
        <div>
            <div className="px-[49px] py-[29px] border-dashed border-x-[1px] border-b-[1px] border-[#F1B290] rounded-b-[25px]">
                <div className="w-full flex text-[#68483C] mb-[18px]">
                    <h3 className="mr-2 text-px16 text-medium">Thời gian : </h3>
                    <div className="font-bold text-px20 leading-[25px]">
                        {/* <p>11h45 AM</p> */}
                        <p>{data.time}</p>

                        {/* <p>22/4/2023</p> */}
                        <p>{data.bookingDate.toISOString()}</p>
                    </div>
                </div>
                <div className="w-full flex text-[#68483C]">
                    <h3 className="mr-2 text-px16 text-medium">Địa điểm :</h3>
                    <div className="font-bold text-px20 leading-[25px]">
                        {/* <p>Spa Yen Hoa</p> */}
                        <p>{data.location.name}</p>
                        {/* <p>36 Yen Hoa, Cau Giay, Vietnam</p> */}
                        <p>{data.location.address}</p>
                    </div>
                </div>
                <div className="text-[#68483C] bg-[#FFE9DD] my-[22px] pl-[26px] pr-[34px] pt-4 pb-8 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-[14px]">
                    <div className="flex justify-between items-center pb-[19px]">
                        <div>

                            {/* <h3 className="font-bold text-px20 leading-[25px] mb-1">CSD Cơ bản</h3> */}
                            <h3 className="sm-480:text-px16 font-bold text-px20 leading-[25px] mb-1">{data.service.title}</h3>

                            {/* <p className="uppercase text-px16 leading-[18px] mb-1">1H</p> */}
                            {/* <p className="uppercase text-px16 leading-[18px]">Phục vụ: Trang Trần</p> */}
                            <p className="uppercase text-px16 leading-[18px]">Phục vụ: {data.staff.name}</p>
                        </div>
                        {/* <p className="text-px20 leading-[25px] font-bold">200.000USD$</p> */}
                        <p className="text-px20 leading-[25px] font-bold">{data.service.price}USD$</p>
                    </div>
                    <div className="flex justify-between items-center text-[#68483C] py-[15px] border-t-[1px]">
                        <p className="text-px16 leading-5">Tổng cộng</p>
                        {/* <p className="text-px20 leading-[25px] font-bold">200.000USD$</p> */}
                        <p className="text-px20 leading-[25px] font-bold">{data.service.price}USD$</p>
                    </div>
                </div>
            </div>
            <div></div>
        </div>
        {/* Content end */}
    </div>
    ) // :<div className="w-full rounded-[10px]"></div>;
}