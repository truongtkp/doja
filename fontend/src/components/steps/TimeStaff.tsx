import { useContext, useEffect ,useState} from "react";
import { StepperContext } from "../../contexts/StepperContext";
import { useTranslation } from "react-i18next";
import Calendar from "react-calendar";


export default function Payment({data,setter}:any) {
    const [t] = useTranslation();

    const { userData, setUserData }: any = useContext(StepperContext);
    const [overlay, setOverlay] = useState<boolean>(false);
    const [date, setDate] = useState(data.bookingDate);
    const [staff, setStaff] = useState({
        total: 0,
        list: []
    });
    const [matrix, setMatrix] = useState([]);
    const [onHighlightedId, setOnHighlightedId] = useState(data.staff.id);
    const [onHighlightedTime, setOnHighlightedTime] = useState(data.time);

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setUserData({ ...userData, [name]: value });
    };


    const dateToLocalTimeISOString = (param:Date) => {
        return new Date(param.getTime() - (param.getTimezoneOffset() * 60000)).toISOString();
    }

    const handleDateChange = (param:any) => {
        setDate(param);
        fetch ("/api/booking/search/date/"+dateToLocalTimeISOString(param).split('T')[0]).then((res) => {
            return res.json();
        }).then((res) => {
            console.log(res);
        });
    }

    useEffect(() => {
        let temp = data;
        temp.callback = () => {
            return (data.staff.id != 0 && data.time != "");
        }
        setter(temp);
        handleDateChange(date);

        fetch ("/api/staff").then((res) => {
            return res.json();
        }).then((res) => {
            setStaff(res);
            staffList();
        });
    },[]);


    const colHandler = (event:any) => {
        const staffId = event.currentTarget.id;
        const time = event.target.parentElement.id;
        data.staff.id = staffId;
        data.time = time;
        data.bookingDate = date;
        setter(data);
        setOnHighlightedId(staffId);
        setOnHighlightedTime(time);
        setOverlay(false);
    }

    const items = () => {
        const pad = (digit:number) => {return (digit<10)? '0' + digit.toString(): digit.toString()};
        const rows:JSX.Element[] = [];

        const setMin = (h:number) => {
            const col = (time:string) => {
                const rows:JSX.Element[] = [];
                const list = staff.list;
                for (let c = 0; c < staff.total;c++){
                    const temp = (list[c] as any);
                    rows.push(
                    <td id={temp.id} onClick={colHandler} className={ (( temp.id == onHighlightedId && 
                        time == onHighlightedTime && 
                        data.bookingDate.getTime() == date.getTime())?
                        "bg-[#E5A380] ":"") +
                    "p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center hover:bg-[#E5A380]"}></td>
                    )
                }
                return rows;
            }

            for (let m = 0; m < 60; m+=30) {
                const time = pad(h) + ":" + pad(m);
                rows.push(
                    <tr id={time} className="bg-white">
                        <td key="" className="p-3 text-sm text-[#64483B] border-[1px] border-[#CECECE] text-center">{time}</td>
                        {col(time)}
                    </tr>
                ) 
            }
        }
        for (let h = 6; h < 24; h++){ // hour
            setMin(h);
        }
        return rows 
    };

    const staffList = () => {
        const rows:JSX.Element[] = [];
        const list = staff.list;
        for (let s = 0; s < staff.total; s++){
            rows.push(
                <th className="min-w-[100px] p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE] text-[#64483B] text-center">{(list[s] as any).name}</th>
            )
        }
        return rows;
    }

    return (
        <div className="">
            <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
            <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description")}</p>

            <div className="rounded-lg shadow overflow-scroll max-h-[405px] max-w-[801px]">
                <table className="w-full rounded-lg">
                    <thead className="sticky top-[-1px] bg-[#F5ECE5] border-2 border-x-gray-200 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)]">
                        <tr>
                            <th 
                                className="p-3 text-sm font-semibold tracking-wide border-[1px] border-[#CECECE]">
                                <div className=" text-[#7E614F] flex flex-col items-center">
                                    {/* <h3 className="text-px20 leading-[25px] whitespace-nowrap">17TH07, 2023</h3> */}
                                    <h3 className="text-px20 leading-[25px] whitespace-nowrap">{date.toDateString()}</h3>
                                    <a href="#" onClick={(e) => {
                                        e.preventDefault();
                                        setOverlay(!overlay)
                                    }}>
                                        <img src="/images/homepages/calender.png" className="w-[30px] mt-2" alt="calender.png"/>
                                    </a>
                                    <div className="relative w-[100%]">
                                    {overlay && (
                                        <div className="w-[20rem] absolute top-1 left-0 items-center bg-white border-2 border-t-border-box">
                                            <Calendar 
                                            value={date} onChange={(e:any) => handleDateChange(e)}
                                            
                                            />
                                        </div>
                                    )}
                                    </div>
                                </div>
                            </th>
                            {staffList()}
                        </tr>

                    </thead>
                    <tbody className="divide-y divide-gray-100">
                        {items()}
                    </tbody>
                </table>
            </div>
        </div>);
}