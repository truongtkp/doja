import { useContext, useEffect, useState } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { StepperContext } from "../../contexts/StepperContext";
import { useTranslation } from "react-i18next";
import locationService from "../../services/location";

export default function Account({data,setter}:any) {
    const [t] = useTranslation();
    const { userData, setUserData }:any = useContext(StepperContext);

    const [address,setAddresses] = useState([]);
    const [onHighlighted,setOnHighlighted] = useState(data.location.id);

    const handleChange = (e:any) => {
        const {name, value} = e.target;
        setUserData({ ...userData, [name]: value});
    };

    useEffect(() => {

        let temp = data;
        temp.callback = () => {
            return data.location.id != 0;
        }
        setter(temp);

        locationService.get({page: 0, limit: 100}).then((res) => {
            setAddresses((res.list as any))
        })
    },[]);

    // const handler = (event:any) => {
    //     // event.target.classList.toggle('active')
    //     // console.log(event);
    //     let temp = onActiveId;
    //     temp = temp++;
    //     setOnActiveId(temp);
    //     // console.log(temp);
    //     // event.target.style.backgroundColor = 'green';
    //     // console.log(event.currentTarget.id)
    // }
    
    const handler = (event:any) => {
        let temp = data;
        temp.location.id = event.currentTarget.id;
        setter(temp);
        setOnHighlighted(event.currentTarget.id);
    }

    const items = () => {
        const rows = [];
        for (let i = 0; i < address.length; i++){
            rows.push(
                <div key={i} id={(address[i] as any).id} onClick={handler} 
                className={ (((address[i] as any).id == onHighlighted)?"bg-[#D1C7BD] ":"") +
                    "text-[#64483B] duration-200 px-[21px] py-[11px] mb-3 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-md border-1 border-[#C4BEBB] hover:text-white hover:bg-[#D1C7BD]"
                }>
                    <h3 className="font-semibold text-px20">{(address[i] as any).name}</h3>
                    <p className="text-px16">{(address[i] as any).address}</p>
                </div>
            ) 
        }
        return rows 
    };

    return (
    <div className="w-full rounded-[10px] shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)] border-1 border-[#C4BEBB]">

        {/* <div className={ onActiveId == 2? "bg-[#D1C7BD]":""} onClick={test}>A</div>
        <div className="bg-[#D1C7BD]">A</div>
        <div className="bg-[#D1C7BD]">A</div>
        <div className="bg-[#D1C7BD]">A</div>
        <div className="bg-[#D1C7BD]">A</div> */}


        <h2 className="absolute top-0 lssm:text-px20 md:text-[32px] text-text-primary font-bold">{t("contact.title")}</h2>
        <p className="absolute top-14 text-text-gray lssm:text-px14 md:text-px16">{t("contact.description")}</p>
        {/* Search Address start */}
        <div className="w-full relative px-[33px] py-[23px] bg-[#F5ECE5] shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-t-[10px]">
        <AiOutlineSearch className="absolute cursor-pointer top-[50%] left-12 md:text-4xl text-2xl translate-y-[-50%] text-[#65493A]" />
        <input
                //   value={inputSearch}
                //   onKeyUp={(event) => {
                //     handleSearchKeyUp(event);
                //   }}
                //   onChange={(event) => ChangeInput(event)}
                  type="text"
                  placeholder=""
                  className="w-full border border-solid rounded-[20px] focus:outline-none 2xl:py-4 xl:py-3 py-2 md:px-20 pl-12 pr-8  border-text-primary text-lg text-black"
                />
        </div>
        {/* Search Address end */}

        {/* Menu Address start */}
        <div className="rounded-b-[10px] bg-[#FAF9F6]">
            <div className="overflow-y-scroll px-[34px] py-[22px] h-[435px]">
                {items()}
                {/* <div className="text-[#64483B] duration-200 px-[21px] py-[11px] mb-3 shadow-[0_4px_4px_0_rgba(0,0,0,0.25)] rounded-md border-1 border-[#C4BEBB] hover:text-white hover:bg-[#D1C7BD]">
                    <h3 className="font-semibold text-px20">Spa Yen Hoa</h3>
                    <p className="text-px16">36 Yen Hoa, Cau Giay, Vietnam</p>
                </div> */}

            </div>
        </div>
        {/* Menu Address end */}
    </div>);
}