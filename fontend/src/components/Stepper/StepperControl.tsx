import React from 'react';

const StepperControl = ({handleClick,currentStep,steps}:any) => {
    return (<div className='container flex justify-end mb-8 px-10'>
        {/* back button */}
        <button
        onClick={() => handleClick()}
        className={`bg-white text-[#65493A] uppercase py-2.5 px-[37px] rounded-xl font-semibold cursor-pointer border-2 border-slate-900 hover:bg-slate-700 hover:text-white transition duration-200 ease-in-out ${currentStep == 1 ? "opacity-50 cursor-not-allowed hidden" : ""}`}>
            Back
        </button>
        {/* next button */}
        <button 
        onClick={() => handleClick("next")}
        className='bg-[#A68276] text-white uppercase py-2.5 ml-2 px-[37px] rounded-xl font-semibold cursor-pointer hover:bg-primary hover:text-white transition duration-200 ease-in-out'>
            {currentStep == steps.length -1 ? "Confirm" : "Next"}
        </button>
    </div>);
};

export default StepperControl;