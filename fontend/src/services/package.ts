import HttpService from "../configs/api";
import getApi from "./getApi";
import { Option, Package, ResponsiveData } from "../types";
import { packageResult, packageTypePost, packageType, searchPackageParam } from "../typeProps/Packagetype";

const PackageManager = {
    get: (option:Option):Promise<ResponsiveData<Package>> => {
        const url = getApi("package");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}})
    },
    getPackageHome: (option:Option):Promise<ResponsiveData<Package>> => {
        const url = getApi("package/home");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    },
    postPackage: (data:packageTypePost):Promise<packageType> => {
        const url = getApi(`package`);
        return HttpService.axiosClient.post(url, data )
    },
    getById: (id:number):Promise<Package> => {
        const url = getApi("package/"+id);
        console.log(url)
        return HttpService.axiosClient.get(url);
    },
    deletePackage:(id:number):Promise<boolean>=> {
        const url = getApi(`package/${id}`);
        return HttpService.axiosClient.delete(url)
    },
    editPackage: (data: any):Promise<Package> => {
        const url = getApi(`package`);
        return HttpService.axiosClient.put(url, data)
    },
    searchPackage:(data: searchPackageParam):Promise<packageResult> => { 
        const url = getApi(`package/search`);
        return HttpService.axiosClient.get(`${url}/${data.type}/${data.keySearch}`,{params: {page: data.option.page, limit: data.option.limit}});
    },
}

export default PackageManager