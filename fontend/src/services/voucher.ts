import HttpService from "../configs/api";
import { Voucher, Option, ResponsiveData } from "../types";
import getApi from "./getApi"


const voucherService = {
    postVoucher: (data:Voucher):Promise<Voucher> => {
        const url = getApi("order-voucher");
        return HttpService.axiosClient.post(url, data)
    },
    editVoucher: (data: Voucher):Promise<Voucher> => {
        const url = getApi(`order-voucher`);
        return HttpService.axiosClient.put(url, data)
    },
    getVouchers: (option:Option)=> {
        const url = getApi("order-voucher");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, size: option.limit}})
    },
    getVoucher: (option:Option)=> {
        const url = getApi("order-voucher");
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, size: option.limit}})
    },
    getVoucherById: (id:string):Promise<Voucher> => {
        const url = getApi(`order-voucher/${id}`);
        return HttpService.axiosClient.get(url);
    },
    orderStatusContact: (data:any):Promise<Voucher> => {
        const url = getApi("order-voucher/status");
        return HttpService.axiosClient.put(url, data)
    },
    deleteVoucher: (id:number):Promise<boolean> => {
        const url =getApi(`order-voucher/${id}`)
        return HttpService.axiosClient.delete(url);
    },
    search: ({keyword, option}:{keyword:string, option:Option}):Promise<ResponsiveData<Voucher>> => {
        const url = getApi("order-voucher/search/"+keyword)
        return HttpService.axiosClient.get(url, {params: {page: option.page - 1, limit: option.limit}});
    }
}


export default voucherService