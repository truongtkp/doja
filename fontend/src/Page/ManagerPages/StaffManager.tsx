import { useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import Pagination from "../../components/Pagination";
import StaffList from "../../containers/Dashboard/Staff/StaffList";
import SteffSearch from "../../containers/Dashboard/Staff/StaffSearh";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import { getStaffReduce, searchStaff, setCurrentPage } from "../../reducers/steff";

const LIMIT = 10;
export default function Career() {
    const steff = useAppSelector(state => state.steff)
    const dispatch = useAppDispatch();
    const [value, setValue] = useState<string>();
    const [t] = useTranslation();

    useEffect(()=> {
        if(value === undefined || value.trim() === "" ) {
            dispatch(getStaffReduce({page: steff.currentPage, limit: LIMIT}))

        }else {
            dispatch(searchStaff({keyword: value, type: 'vi' ,option: {page: steff.currentPage, limit: LIMIT}}))
        }
    }, [steff.currentPage]);
    
    const total = useMemo(()=> {

        return Math.ceil(steff.total/LIMIT);
    }, [steff.total])

    return (
        <div className="w-full ">
             <h2 className="uppercase text-[48px] sc>768:text-px20 text-text-primary font-bold text-center lssm:py-[40px] md:py-[74px]">{t("dashboard.recruitment.titlestaff")}</h2>
              <SteffSearch value={value ?? ""} setValue={setValue} />
              <StaffList />
              <div className="sc>768:mb-[32px]">
              {
                total > 0 && (
                    <Pagination currenPage={steff.currentPage} setCurrentPage={setCurrentPage} total={total} />
                )
              }
            </div>
        </div>
    )
}