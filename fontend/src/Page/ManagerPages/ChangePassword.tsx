import React from "react";
import TitlePage from "../../components/ManagerComponent/TitlePage";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useAppDispatch, useAppSelector } from "../../hooks/hook";
import userService from "../../services/user";
import { pushPopup } from "../../reducers/popupSlice";
import { useNavigate } from "react-router-dom";
import { hostBE } from "../../types/host";

function ChangePassword() {
  const user = useAppSelector((state) => state.userSlice.currentUser);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
    },
    validationSchema: Yup.object({
      currentPassword: Yup.string().required("Bắt buộc"),
      newPassword: Yup.string()
        .required("Bắt buộc")
        .matches(
          /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
          "Mật khẩu phải có ít nhất 1 chữ hoa, 1 ký tự đặc biệt, số"
        )
        .min(8, "Tên người dùng tối thiểu 8 ký tự")
        .when("currentPassword", {
          is: (val: any) => (val && val.length > 0 ? true : false),
          then: Yup.string().notOneOf(
            [Yup.ref("currentPassword")],
            "Mật khẩu mới đã trùng với mật khẩu cũ, vui lòng nhập lại"
          ),
        }),
      confirmPassword: Yup.string().when("newPassword", {
        is: (val: any) => (val && val.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("newPassword")],
          "Mật khẩu không trừng khớp"
        ),
      }),
    }),
    onSubmit: (values) => {
      userService
        .changePassword({
          currentPassword: values.currentPassword,
          newPassword: values.newPassword,
        }).then(()=> {
          dispatch(pushPopup({
            message: "Đổi mật khẩu thành công",
            type: "SUCCESS"
          }))
         navigate("/quanly/thongtintaikhoan")
        }).catch((error)=> {
          if(error.response.data.title === "Password different") {
            dispatch(pushPopup({
              message: "Mật khẩu không đúng!",
              type: "ERROR"
            }))
          }else {

            dispatch(pushPopup({
              message: "Đổi mật khẩu thất bại",
              type: "ERROR"
            }))
          }

        })
    },
  });

  const goBack = () => {
    navigate(-1);
  };

  return (
    <form
      onSubmit={formik.handleSubmit}
      className="w-full px-[10px] mb-[120px]"
    >
      <TitlePage content="titleManager.changePassTitle" />
      <div className="w-full">
        <div className="flex items-center mb-16">
          <div className="xl:w-[140px]  xl:h-[140px] sm-390:w-[100px] sm-390:h-[100px] w-[60px] h-[60px] rounded-[50%] relative flex items-center justify-center">
            <img
              className="rounded-[50%] w-full h-full"
              src={`${hostBE}/fe/loginImage.png`}
              alt="accountImage"
            />
            <img
              className="rounded-[50%] absolute top-auto translate-y-[0%] w-[85%] h-[85%] object-cover"
              src={user?.avatarUrl ?? `${hostBE}/fe/logotitle.png`}
              alt="avataracount"
            />
          </div>
          <div className="h-full flex-col flex sm-390:ml-[30px] ml-6">
            <p className="sm-480:text-2xl sm-390:text-xl text-[15px] font-bold sm-390:mb-5">
              {user?.fullname}
            </p>
            <p className="sm-390:text-base text-xs font-normal sm-390:mb-5">
              {user?.position}
            </p>
          </div>
        </div>
      </div>
      <div className="w-full">
        <div className="w-full flex flex-col mb-6">
          <label className="text-text-primary font-bold lg:text-px16 text-xs lg:mb-4 mb-[6px]">
            Mật khẩu cũ <span className="text-[#EB0000]">*</span>
          </label>
          <input
            className="w-full px-3 py-3 border-border-gray border rounded-md outline-none focus:border-primary"
            type="password"
            value={formik.values.currentPassword}
            onChange={formik.handleChange}
            name="currentPassword"
          />
          <small className="text-px14 text-[#fe6a38]">
            {formik.errors.currentPassword}
          </small>
        </div>
        <div className="w-full flex flex-col mb-6">
          <label className="text-text-primary font-bold lg:text-px16 text-xs lg:mb-4 mb-[6px]">
            Mật khẩu mới <span className="text-[#EB0000]">*</span>
          </label>
          <input
            className="w-full px-3 py-3 border-border-gray border rounded-md outline-none focus:border-primary"
            type="password"
            value={formik.values.newPassword}
            onChange={formik.handleChange}
            name="newPassword"
          />
          <small className="text-px14 text-[#fe6a38]">
            {formik.errors.newPassword}
          </small>
        </div>

        <div className="w-full flex flex-col mb-6">
          <label className="text-text-primary font-bold lg:text-px16 text-xs lg:mb-4 mb-[6px]">
            Nhập lại mật khẩu <span className="text-[#EB0000]">*</span>
          </label>
          <input
            className="w-full px-3 py-3 border-border-gray border rounded-md outline-none focus:border-primary"
            type="password"
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
            name="confirmPassword"
          />
          <small className="text-px14 text-[#fe6a38]">
            {formik.errors.confirmPassword}
          </small>
        </div>
      </div>
      <div className="flex justify-end  xl:mt-[120px] mt-[60px] m992:mb-[60px] lg:mb-0">
        <button
          onClick={goBack}
          type="button"
          className="py-[13px] mr-[10px] sm-390:px-6 px-0 sm-480:w-auto w-[48%] border rounded-md border-text-lightred text-text-lightred font-medium lg:text-xl sm-390:text-lg text-sm text-center"
        >
          Hủy
        </button>
        <button
          type="submit"
          className="py-[13px]  sm-390:px-6 px-0 sm-480:w-auto w-[48%] font-medium border rounded-md bg-primary text-white lg:text-xl sm-390:text-lg text-sm text-center"
        >
          Lưu thiết lập
        </button>
      </div>
    </form>
  );
}

export default ChangePassword;
