import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Voucher, Option, ResponsiveData } from '../types'
// import {ModalState} from "../types"

// Define a type for the slice state
interface AuthState {
   voucherList: Voucher[]
   isLoading: boolean,
   total: number,
   currentPage: number
}

// Define the initial state using that type
const initialState: AuthState = {
    voucherList: [],
    isLoading: false,
   total: 0,
   currentPage: 1

}

export const voucherSlice = createSlice({
  name: 'voucher',
  initialState,
  reducers: {
    setCurrentPage: (state, action:PayloadAction<number>) => {
        state.currentPage = action.payload
    },
        getVoucher: (state, action:PayloadAction<Option>) => {
            state.isLoading = true;
        },
        getVoucherSuccess: (state, action:PayloadAction<ResponsiveData<Voucher>>) => {
            state.voucherList = action.payload.list
            state.total = action.payload.total
            state.isLoading = false;
        },
        getVoucherFail: (state) => {
            state.isLoading = false;
        },
        orderStatusSuccess: (state, action:PayloadAction<Voucher>)=> {
            const index = state.voucherList.findIndex(voucher => voucher.id === action.payload.id);
            state.voucherList.splice(index, 1, action.payload);
        },
        deleteVoucherReduce: (state, action:PayloadAction<number>) => {
            state.isLoading = true;
        },
        deleteVoucherSuccess: (state, action:PayloadAction<number>)=> {
            state.voucherList = state.voucherList.filter(voucher => voucher.id !== action.payload);
            state.isLoading = false
        },
        deleteVoucherFail: (state) => {
            state.isLoading = false;
        },
        searchVoucher: (state, action:PayloadAction<{keyword:string, option:Option}>) => {
            state.isLoading = true;
        },
        searchVoucherSuccess: (state, action:PayloadAction<ResponsiveData<Voucher>>) => {
            state.voucherList = action.payload.list
            state.total = action.payload.total
            state.isLoading = false;
        },
        searchVoucherFail: (state) => {
            state.isLoading = false
        }
  
  },
})

export const {getVoucher, getVoucherSuccess, getVoucherFail,
    orderStatusSuccess, deleteVoucherReduce, deleteVoucherSuccess,
    deleteVoucherFail, setCurrentPage,
    searchVoucher, searchVoucherSuccess, searchVoucherFail
    } = voucherSlice.actions

export default voucherSlice.reducer