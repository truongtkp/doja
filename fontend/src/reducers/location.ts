import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Option, Location, ResponsiveData } from "../types";



// Define a type for the slice state
interface LoactionState {
    location: Location[],
    currentPage: number
    total: number,
    isLoading: boolean
    locationListActive: Location[]
    locationHome: Location[],
    currentPageHome: number,
    totalHome: number,
}

// Define the initial state using that type
const initialState: LoactionState = {
    location: [],
    currentPage: 1,
    total: 0,
    isLoading: false,
    locationListActive: localStorage.getItem("location") ? JSON.parse(localStorage.getItem("location")+"") : [],
    locationHome: [],
    currentPageHome: 0,
    totalHome: 12,
};

export const locationSlice = createSlice({
  name: "location",
  initialState,
  reducers: {
    setCurrentPage: (state, action:PayloadAction<number>) => {
        state.currentPage = action.payload;
    },
    setCurrentPageHome: (state, action:PayloadAction<number >) => {
        state.currentPageHome = action.payload
    }
    ,
   getLocationActive: (state) => {
      
    },
    
    getLocation: (state, option:PayloadAction<Option>) => {
        state.isLoading = true
    },
    getLocationSuccess: (state, option:PayloadAction<ResponsiveData<Location>>) => {
        state.location = option.payload.list;
        state.total = option.payload.total;
        state.isLoading = false
    },
    getLocationFail: (state) => {
        state.isLoading = false
    },
    getLocationHome: (state, action:PayloadAction<Option>) => {
        
    },
    getLocationHomeSuccess: (state, action:PayloadAction<ResponsiveData<Location>>) => {
        state.totalHome = action.payload.total
        state.locationHome = [...state.locationHome, ...action.payload.list];
    }
  },
});

export const {setCurrentPage,getLocation, getLocationFail, getLocationSuccess,
    getLocationActive,
    getLocationHome, getLocationHomeSuccess,
    setCurrentPageHome
             
} =
  locationSlice.actions;

export default locationSlice.reducer;
