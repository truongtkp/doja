import { Option } from "../types";

export type searchBookingParam = {
    type: string;
    keySearch: string;
    option: Option;
  }

  export type bookingType = {
    id: number
    address: string
    servicePackage: string
    staff: string
    timetable: string
    bookingDate: string
    fullName: string
    email: string
    phoneNo: string
    orderId: string
  }

  export type  typeBookingEdit = {
    address: string
    servicePackage: string
    staff: string
    timetable: string
    bookingDate: string
    fullName: string
    email: string
    phoneNo: string
    orderId: string
  }

  export type bookingTypePost = {
    address: string
    servicePackage: string
    staff: string
    timetable: string
    bookingDate: string
    fullName: string
    email: string
    phoneNo: string
    orderId: string
  }

  export type bookingResult = {total:number, list: bookingType[]}