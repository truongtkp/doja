import { Option } from "../types";
export type ProductItems = {
  id: number;
  categoryId: number;
  avatarUrl: string;
  avatarPath: string;
  descriptionEn: string;
  descriptionVi: string;
  contentVi: string;
  contentEn: string;
  titleEn: string;
  titleVi: string;
  priority: boolean;
  specificationList: specificationsType[];
  productImageList: productimageType[];
};

export type  typeProductEdit = {
  categoryId: number | string;
  avatarUrl: string;
  avatarPath: string;
  descriptionEn: string;
  descriptionVi: string;
  titleEn: string;
  titleVi: string;
  priority: boolean;
  specificationList: specificationsType[];
}

export type ProducPublicType = {
  id: number;
  avatarUrl: string;
  descriptionEn: string;
  descriptionVi: string;
  titleEn: string;
  titleVi: string;
  activated: boolean;
  priority?: boolean;
};

export type specificationsType = {
  id?: number;
  nameEn: string;
  nameVi: string;
  specificationDetailList: specificationDetailsType[];
};
export type specificationDetailsType = {
  id?: number;
  propertiesEn: string;
  propertiesVi: string;
  valueVi: string;
  valueEn: string;
};

export type productimageType = {
  id: number;
  imageUrl: string;
  imagePath: string;
};

export type categoryType = {
  createdBy: string;
  createdDate: string;
  id: number;
  nameEn: string;
  nameVi: string;
  descriptionEn: string,
  descriptionVi: string,
  imagePath?: string,
  imageUrl?: string,
  activated: boolean;
};
export type categoryPost = {
  nameEn: string;
  nameVi: string;
  descriptionEn: string,
  descriptionVi: string,
  imagePath: string,
  imageUrl: string
};
export type resultProductPublic = {
  total:number;
  list: ProducPublicType[]
}


export type productParam = {
  keySearch: string;
  option: Option;
}

export type productPublicType = {
  type: string;
  search: string|null;
  categoryId:number;
  page:number;
  limit:number;
  sort:number;
}

// projecttype


export type searchProjecParam = {
  type: string;
  keySearch: string;
  option: Option;
}

export type projecType = {
  id: number;
  titleVi: string;
  titleEn: string;
  descriptionVi: string;
  descriptionEn: string;
  avatarUrl: string;
  priority: boolean;
  createdDate:string;
}


export type projectTypePost = {
  titleVi: string,
  titleEn: string,
  descriptionVi: string,
  descriptionEn: string,
  contentVi: string,
  contentEn: string,
  avatarUrl: string,
  avatarPath: string,
}
export type projecResult = {total:number, list: projecType[]}
export type productTypeState = { total: number; list: ProducPublicType[] }