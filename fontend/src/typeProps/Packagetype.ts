import { Option } from "../types";

export type searchPackageParam = {
    type: string;
    keySearch: string;
    option: Option;
  }
  
  export type packageType = {
    id: number;
    title: string,
    price: string,
    description: string
  }
  
  export type packageTypePost = {
    title: string,
    price: string,
    description: string
  }
  
  export type packageResult = {total:number, list: packageType[]}