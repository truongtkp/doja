

export type Recruit = {
    id?: number
    titleEn: string
    titleVi: string
    workplaceVi: string
    workplaceEn: string
    dateExpiration: string
    describeVi: string
    interestVi: string
    requireVi: string
    describeEn: string
    interestEn: string
    requireEn:string
    salaryEn:string
    salaryVi:string
    numPerson:string
    levelEn: string
    levelVi: string
    genderVi: string
    genderEn: string
    experienceVi: string
    experienceEn: string
    workingFormEn: string
    workingFormVi: string
    avatarUrl: string
    avatarPath:string
    
}


export type Candidate = {
    id?:number
    fullname: string
    gender: boolean
    email:string
    address: string
    education:string
    experience: string
    phone: string
    birthday: string
    goal: string
    presenter: string
    cvUrl: string,
    cvPath?: string,
    recruit?: Recruit
    createdDate?:string
    createdBy?:string
}