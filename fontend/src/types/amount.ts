export type Amount = {   
    id:number,
    name: string,
    price: string,
    description: string
}