export type Staff = {
    id?: number,
    name: string,
    image: string,
    birthday: string,
    email: string,
    phoneNo: string,
    description: string,
    address: string
}
