export type Album = {
    id?: number
    descriptionVi: string
    descriptionEn: string,
    topicImage?: TopicImage
}

export type TopicImage = {
    id?:number
    imageUrl: string,
    imagePath: string
}