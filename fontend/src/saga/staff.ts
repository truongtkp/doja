import { Option, Staff, ResponsiveData } from './../types';
import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest, debounce, select } from 'redux-saga/effects';
import { deleteStaff, deleteStaffFail, deleteStaffSuccess, getStaffFail, getStaffReduce, getStaffSuccess, searchStaff, searchStaffFail, searchStaffSuccess, setCurrentPage, updateStaff, updateStaffFail, updateStaffSuccess } from '../reducers/steff';
import StaffManager from '../services/staff';
import { pushPopup } from '../reducers/popupSlice';
import { hideModal } from '../reducers/modal';
import { RootState } from '../configs/redux/store';
import { setCurrenPage } from '../reducers/productPublic';
import { TranslateState } from '../reducers/translate';




function* getStaffMethod ({payload}:PayloadAction<Option>) {
    try {
        const res:ResponsiveData<Staff> = yield call(StaffManager.get, payload);
        yield put(getStaffSuccess(res))


    }catch(error) {
        yield put(getStaffFail());
        
    }

}


function* updateStaffMethod({payload}:PayloadAction<Staff>) {
    try {
        const res:Staff = yield call(StaffManager.editStaff, payload);
        yield put(updateStaffSuccess(res));

    }catch(error) {
        yield put(updateStaffFail());
        
    }


}


function* searchStaffSaga({payload}:PayloadAction<{keyword:string, type:string, option:Option}>) {
    try {
        if(payload.keyword.trim() !== "") {
            const res:ResponsiveData<Staff> = yield call(StaffManager.search, payload);
            yield put(searchStaffSuccess(res));

        }else {
            yield put(getStaffReduce(payload.option));
        }
    }catch(error) {
        yield put(searchStaffFail());

    }
}

function* deleteStaffSaga({payload}:PayloadAction<number>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);  

    try {
        const res:boolean  = yield call(StaffManager.deleteStaff, payload);
        if(res === true) {
            const {listStaff, currentPage}= yield select((state:RootState) => state.steff);
            if(listStaff.length === 1 && currentPage !== 1 ) {
                yield put(setCurrentPage(currentPage - 1))
            }else {
                    yield put(getStaffReduce({
                        page: currentPage,
                        limit: 10
                    }))
            }
            yield put(deleteStaffSuccess(payload));
            yield put(pushPopup({
                message: translate.isEnglish ? "Xóa thành công" : "Delete successfully",
                type: "SUCCESS"
            }))
            yield put(hideModal())
        }else {
            yield put(deleteStaffFail())
            yield put(pushPopup({
                message: translate.isEnglish ? "Xóa thất bại" : "Delete failed",
                type: "ERROR"
            }))
            yield put(hideModal())

        }
    }catch(error) {
        yield put(deleteStaffFail())
        yield put(pushPopup({
            message: translate.isEnglish ? "Xóa thất bại" : "Delete failed",
            type: "ERROR"
        }))
        yield put(hideModal())
    }
}



export default function* staffSaga() {
    yield takeLatest(getStaffReduce.type, getStaffMethod)
    yield takeLatest(updateStaff.type, updateStaffMethod)
    yield takeLatest(deleteStaff.type, deleteStaffSaga)
    yield debounce(1000,searchStaff.type, searchStaffSaga)
}