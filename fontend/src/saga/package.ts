import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { RootState } from '../configs/redux/store';
import { hideModal } from '../reducers/modal';
import { pushPopup } from '../reducers/popupSlice';
import { editPackageSuccess, editPackage, getPackage, getPackageFail, getPackageSuccess, searchPackage, searchPackageSuccess } from '../reducers/packagemanagerSlice';
import { TranslateState } from '../reducers/translate';
import packageServiece from '../services/package';
import { packageResult, packageType, searchPackageParam } from '../typeProps/Packagetype';
import { Option } from './../types';


function* getPackageSaga({payload}: PayloadAction<Option>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: packageResult = yield call(packageServiece.get,payload);
        yield put(getPackageSuccess(res))
    } catch (error) {
        yield put(getPackageFail(isEnglish ? "Không hiển thị được danh sách gói dịch vụ" : "Unable to display packages list"))
    }
}

function* searchPackageSaga({payload}: PayloadAction<searchPackageParam>){
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res : packageResult = yield call(packageServiece.searchPackage, payload);
        yield put(searchPackageSuccess(res))
        
    } catch (error) {
        yield put(getPackageFail(isEnglish ? "Không tìm thấy gói dịch vụ nào" : "No packages found"))
    }
}

function* editPackageSaga({payload}: PayloadAction<packageType>) {
    const translate:TranslateState  = yield select((state:RootState) => state.translateSlice);
    const {isEnglish} = translate;
    try {
        const res: packageType = yield call(packageServiece.editPackage,payload);
        yield put(editPackageSuccess(res))
        yield put( pushPopup({
            type: "SUCCESS",
            message: isEnglish ? "Sửa gói dịch vụ thành công." : "Successful packages editing.",
          }))

          yield put(hideModal());
    } catch (error) {
        yield put(
            pushPopup({
                type: "WARNING",
                message: isEnglish ? "Không sửa được gói dịch vụ!" : "Unable to edit packages!",
              })
        )
    }
}



export default function* projectManagerSagaMid(){
    yield takeLatest(getPackage.type,getPackageSaga);
    yield takeLatest(searchPackage.type,searchPackageSaga);
    yield takeLatest(editPackage.type,editPackageSaga);
}