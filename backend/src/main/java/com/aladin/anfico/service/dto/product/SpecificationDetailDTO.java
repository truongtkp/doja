package com.aladin.anfico.service.dto.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SpecificationDetailDTO {

    private Long id;

    @Size(max = 200)
    private String propertiesEn;

    @Size(max = 200)
    private String propertiesVi;

    @Size(max = 500)
    private String valueVi;

    @Size(max = 500)
    private String valueEn;

}
