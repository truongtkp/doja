package com.aladin.anfico.service.mapper.product;

import com.aladin.anfico.domain.Product;
import com.aladin.anfico.service.dto.product.ProductDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
}
