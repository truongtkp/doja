package com.aladin.anfico.service.mapper.recruit;

import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.RecruitDTO;
import com.aladin.anfico.service.dto.recruit.RecruitFillterDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Recruit} and its DTO {@link RecruitDTO}.
 */
@Mapper(componentModel = "spring")
public interface RecruitMapper extends EntityMapper<RecruitDTO, Recruit> {

}
