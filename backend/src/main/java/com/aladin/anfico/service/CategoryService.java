package com.aladin.anfico.service;

import com.aladin.anfico.domain.Category;
import com.aladin.anfico.service.dto.category.CategoryDTO;
import java.util.HashMap;
import java.util.Optional;

public interface CategoryService {
    Category addCategory(CategoryDTO categoryDTO);

    Optional<Category> updateCategory(CategoryDTO categoryDTO);

    Optional<Category> deleteCategory(Long id);

    HashMap<String, ?> getAllManagedCategory();
}
