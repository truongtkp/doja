package com.aladin.anfico.service;

import com.aladin.anfico.service.dto.file.FileDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

public interface FileService {
    HashMap<String, ?> uploadImage(MultipartFile[] file) throws IOException;

    FileDTO uploadFile(MultipartFile file) throws IOException;

    Resource downloadFile(String param) throws IOException;

    boolean deleteFile(String pathFile);
}
