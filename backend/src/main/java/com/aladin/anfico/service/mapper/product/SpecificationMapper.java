package com.aladin.anfico.service.mapper.product;

import com.aladin.anfico.domain.Specification;
import com.aladin.anfico.domain.SpecificationDetail;
import com.aladin.anfico.service.dto.product.SpecificationDTO;
import com.aladin.anfico.service.dto.product.SpecificationDetailDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link SpecificationDetail} and its DTO {@link SpecificationDetailDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpecificationMapper extends EntityMapper<SpecificationDTO, Specification> {
}