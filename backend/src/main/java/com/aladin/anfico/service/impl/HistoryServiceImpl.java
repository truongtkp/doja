package com.aladin.anfico.service.impl;


import com.aladin.anfico.domain.History;
import com.aladin.anfico.repository.HistoryRepository;
import com.aladin.anfico.service.FileService;
import com.aladin.anfico.service.HistoryService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.history.HistoryDTO;
import com.aladin.anfico.service.mapper.history.HistoryMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {

    private final Logger log = LoggerFactory.getLogger(HistoryServiceImpl.class);
    private final HistoryMapper historyMapper;
    private final HistoryRepository historyRepository;
    private final FileService fileServiceImpl;


    /**
     * {@Create} information in history table.
     *
     * @param historyDTO to DTO
     * @return History
     */
    @Override
    public History addHistory(HistoryDTO historyDTO){
        log.info("[Anfico App]: add {} history", historyDTO.getYear());
        History h = historyMapper.toEntity(historyDTO);
        return historyRepository.save(h);
    }


    /**
     * {@Update} information in history table.
     *
     * @param historyDTO to DTO
     * @return Optional<History>
     */
    @Override
    public Optional<History> updateHistory(HistoryDTO historyDTO){
        return Optional
            .of(historyRepository.findById(historyDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(h -> {
                if (!h.getImagePath().equals(historyDTO.getImagePath())){
                    fileServiceImpl.deleteFile(h.getImagePath());
                }
                historyMapper.partialUpdate(h, historyDTO);
                log.info("[Anfico App]: update {} history", historyDTO.getYear());
                return historyRepository.save(h);
            });
    }


    /**
     * {@Delete} information in history table.
     *
     * @param id type long
     * @return Optional<History>
     */
    @Override
    public Optional<History> deleteHistory(Long id){
        return
            Optional
                .of(historyRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(history -> {
                    fileServiceImpl.deleteFile(history.getImagePath());
                    historyRepository.delete(history);
                    log.info("[Anfico App]: delete {} history success", history.getYear());
                    return history;
                });
    }


    /**
     * {@Read} information in history table.
     *
     * @param page type int
     * @param size type int
     * @return HashMap<String,?>
     */
    @Override
    public HashMap<String,?> getAllManagedHistory(int page, int size) {
        log.info("[Anfico App]: Get all a history");
        return ArrayDTO.convertListToSet(historyRepository.count(),historyRepository
            .findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))).map(HistoryDTO::new).toList());
    }


    /**
     * {@Read} detail information in history table.
     *
     * @param id type Long
     * @return Optional<History>
     */
    @Override
    public Optional<History> getHistory(Long id){
        return
                Optional
                        .of(historyRepository.findById(id))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .map(history -> {
                            log.info("[Anfico App]: get {} history ", history.getYear());
                            return history;
                        });
    }
}
