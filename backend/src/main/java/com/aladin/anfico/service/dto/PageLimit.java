package com.aladin.anfico.service.dto;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springdoc.core.converters.models.Pageable;

import javax.validation.constraints.Min;

public class PageLimit {
    @Parameter(
        description = "Zero-based page index (0..N)",
        schema = @Schema(
            type = "integer",
            defaultValue = "0"
        )
    )
    private @Min(0L) Integer page;
    @Parameter(
        description = "The size of the page to be returned",
        schema = @Schema(
            type = "integer",
            defaultValue = "20"
        )
    )
    private @Min(1L) Integer limit;
}
