package com.aladin.anfico.service.mapper.history;

import com.aladin.anfico.domain.History;
import com.aladin.anfico.service.dto.history.HistoryDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link History} and its DTO {@link HistoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface HistoryMapper extends EntityMapper<HistoryDTO, History> {

}