package com.aladin.anfico.service.mapper.product;

import com.aladin.anfico.domain.Product;
import com.aladin.anfico.service.dto.product.ProductCreateDTO;
import com.aladin.anfico.service.dto.product.ProductCrudDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductCrudDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductCreateMapper extends EntityMapper<ProductCreateDTO, Product> {
}

