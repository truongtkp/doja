package com.aladin.anfico.service;

import com.aladin.anfico.domain.Candidate;
import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface RecruitService {

    Optional<RecruitDTO> addRecruit(RecruitCreateDTO recruitCreateDTO);

    Optional<RecruitDTO> updateRecruit(RecruitCreateDTO recruitCreateDTO);

    Optional<Recruit> deleteRecruit(Long id);

    HashMap<String, ?> getAllManagedRecruit(int page, int size);

    List<RecruitFillterDTO> getAllFillterRecruit();

    Optional<Recruit> getRecruitById(Long id);

    HashMap<String, ?> getCandidateByRecruit(Long id, int page, int size);

    Page<Candidate> findAllCandidateByRecruitId(Long id, Pageable pageable);

    Optional<Candidate> getCandidateById(Long id);

    HashMap<String, ?> getCandidate(int page, int size);

    Optional<Candidate> createCandidateByRecruit(Recruit recruit, CandidateDTO candidateDTO);

    Optional<Candidate> isStatus(CandidateStatusDTO candidateStatusDTO);

    HashMap<String, ?> searchRecruitEn(String param, int page, int size);

    HashMap<String, ?> searchRecruitVi(String param, int page, int size);


}


