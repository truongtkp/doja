package com.aladin.anfico.service.dto.recruit;

import com.aladin.anfico.domain.Recruit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecruitCreateDTO {


    private Long id;

    @NotNull
    @Size(max = 500)
    private String titleEn;

    @NotNull
    @Size(max = 500)
    private String titleVi;

    @NotNull
    @Size(max = 500)
    private String workplaceVi;

    @NotNull
    @Size(max = 500)
    private String workplaceEn;

    @NotNull
    private Date dateExpiration;

    @NotNull
    private String describeVi;

    @NotNull
    private String interestVi;

    @NotNull
    private String requireVi;

    @NotNull
    private String describeEn;

    @NotNull
    private String interestEn;

    @NotNull
    private String requireEn;


    @NotNull
    @Size(max = 256)
    private String salaryVi;

    @NotNull
    @Size(max = 256)
    private String salaryEn;

    @NotNull
    private int numPerson;


    @NotNull
    @Size(max = 256)
    private String levelVi;

    @NotNull
    @Size(max = 256)
    private String levelEn;

    @NotNull
    @Size(max = 40)
    private String genderVi;

    @NotNull
    @Size(max = 40)
    private String genderEn;


    @NotNull
    @Size(max = 100)
    private String experienceVi;

    @NotNull
    @Size(max = 100)
    private String experienceEn;

    @NotNull
    @Size(max = 100)
    private String  workingFormVi;

    @NotNull
    @Size(max = 100)
    private String  workingFormEn;


    @NotNull
    @Size(max = 500)
    private String avatarUrl;

    @NotNull
    @Size(max = 500)
    private String avatarPath;


}
