package com.aladin.anfico.service.dto.user;

import com.aladin.anfico.config.Constants;
import lombok.Data;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.*;

@Data
public class UserKeycloakDTO {

    private String id;

    private String login;

    @Size(max = 50)
    private String fullname;

    @Email
    @Size(min = 5, max = 256)
    private String email;

    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;

    private Long birth;

    private Boolean gender;

    @Value("${image.url}")
    private String avatarUrl;

    @Value("${image.path}")
    private String avatarPath;

    private String position;

    private Map<String, String> authorities = new HashMap<>();

    private Long createdDate;

    private boolean enabled;

    public UserKeycloakDTO(UserRepresentation userRepresentation,  List<RoleRepresentation> roles) {
        this.id = userRepresentation.getId();
        this.login = userRepresentation.getUsername();
        this.fullname = userRepresentation.getFirstName();
        this.email = userRepresentation.getEmail();
        this.createdDate =userRepresentation.getCreatedTimestamp();
        this.enabled = userRepresentation.isEnabled();
        roles.forEach(roleRepresentation -> {
            authorities.put("name", roleRepresentation.getName());
        });


        if (roles == null || roles.size() ==0) this.enabled = false;
        Map<String, List<String>> map = userRepresentation.getAttributes();
        if (map != null) {
            this.phone = map.containsKey("phone") ? map.get("phone").get(0) : null;
            this.birth = map.containsKey("birth") ? Long.parseLong(map.get("birth").get(0))  : null;
            this.gender = map.containsKey("gender") ? Boolean.parseBoolean(map.get("gender").get(0)) : false;
            this.avatarUrl = map.containsKey("avatarUrl") ? map.get("avatarUrl").get(0) : null;
            this.avatarPath = map.containsKey("avatarPath") ? map.get("avatarPath").get(0) : null;
            this.position = map.containsKey("position") ? map.get("position").get(0) : null;
        }else{
            this.avatarUrl= "https://www.anfico.vn/image/avatar.jpg";
//            this.avatarUrl= "http://101.99.6.31:9090/image/avatar.jpg";
        }
    }

}
