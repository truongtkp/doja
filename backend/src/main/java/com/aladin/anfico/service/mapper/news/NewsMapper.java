package com.aladin.anfico.service.mapper.news;

import com.aladin.anfico.domain.History;
import com.aladin.anfico.domain.News;
import com.aladin.anfico.service.dto.history.HistoryDTO;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link News} and its DTO {@link NewsCreateDTO}.
 */
@Mapper(componentModel = "spring")
public interface NewsMapper extends EntityMapper<NewsCreateDTO, News> {

}

