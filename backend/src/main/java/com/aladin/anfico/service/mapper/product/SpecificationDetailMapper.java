package com.aladin.anfico.service.mapper.product;

import com.aladin.anfico.domain.Product;
import com.aladin.anfico.domain.SpecificationDetail;
import com.aladin.anfico.service.dto.product.ProductDTO;
import com.aladin.anfico.service.dto.product.SpecificationDetailDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity {@link SpecificationDetail} and its DTO {@link SpecificationDetailDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpecificationDetailMapper extends EntityMapper<SpecificationDetailDTO, SpecificationDetail> {
}