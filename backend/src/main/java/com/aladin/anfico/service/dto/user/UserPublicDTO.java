package com.aladin.anfico.service.dto.user;

import com.aladin.anfico.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.keycloak.representations.idm.UserRepresentation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserPublicDTO {

    @NotNull
    private String id;
    @NotNull
    private String fullname;

    @Pattern(regexp = Constants.PHONE_REGEX)
    private String phone;
    @NotNull
    private Date birth;
    @NotNull
    private boolean gender;
    private String avatarUrl;
    private String avatarPath;
    @NotNull
    private String position;

    public UserRepresentation createUserRepresentation( UserRepresentation userRepresentation){

        userRepresentation.setFirstName(getFullname());
        userRepresentation.setEnabled(true);
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if (userRepresentation.getAttributes() == null)
            attributes= new HashMap<>();
        attributes.put("phone", Collections.singletonList(getPhone()));
        attributes.put("birth", Collections.singletonList(String.valueOf(getBirth().getTime())));
        attributes.put("position", Collections.singletonList(getPosition()));
        attributes.put("gender" , Collections.singletonList(String.valueOf(isGender())));
        attributes.put("avatarUrl" , Collections.singletonList(String.valueOf(getAvatarUrl())));
        attributes.put("avatarPath" , Collections.singletonList(String.valueOf(getAvatarPath())));

        userRepresentation.setAttributes(attributes);

        return userRepresentation;
    }


}
