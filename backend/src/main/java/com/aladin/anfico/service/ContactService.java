package com.aladin.anfico.service;

import com.aladin.anfico.domain.Contact;
import com.aladin.anfico.service.dto.contact.ContactCreateDTO;
import com.aladin.anfico.service.dto.contact.ContactUpdateDTO;

import java.util.HashMap;
import java.util.Optional;

public interface ContactService {

    Optional<ContactCreateDTO> addContact(ContactCreateDTO contactCreateDTO);

    HashMap<String, ?> getAllContact(int page, int size);

    Optional<Contact> getContactById(Long id);

    Optional<Contact> deleteContactById(Long id);

    Optional<Contact> updateContact(ContactUpdateDTO contactUpdateDTO);

    HashMap<String, ?> searchContactByAll(String param, int page, int size);
}
