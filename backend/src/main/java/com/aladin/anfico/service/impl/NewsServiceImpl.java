package com.aladin.anfico.service.impl;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.News;
import com.aladin.anfico.repository.NewsRepository;
import com.aladin.anfico.service.NewsService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.service.dto.news.NewsCreateDTO;
import com.aladin.anfico.service.mapper.news.NewsMapper;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NewsServiceImpl implements NewsService {

    private final Logger log = LoggerFactory.getLogger(NewsServiceImpl.class);
    private final NewsRepository newsRepository;
    private final FileServiceImpl fileServiceImpl;
    private final NewsMapper newsMapper;
    @Override
    public News createNews(NewsCreateDTO news){
        log.info("--Create a news table --");
        return newsRepository.save(newsMapper.toEntity(news));
    }


    public Optional<News> updateNews(NewsCreateDTO newsCreateDTO){
        log.info("--Create a news table --");
        return Optional
            .of(newsRepository.findById(newsCreateDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(news -> {
                if(!newsCreateDTO.getAvatarPath().equals(news.getAvatarPath())){
                    fileServiceImpl.deleteFile(news.getAvatarPath());
                }
                newsMapper.partialUpdate(news,newsCreateDTO);
                return newsRepository.save(news);
            });
    }
    @Override
    public Optional<News> deleteNews(Long id){

        return Optional
                .of(newsRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(news -> {
                    fileServiceImpl.deleteFile(news.getAvatarPath());
                    newsRepository.delete(news);
                    return news;
                });
    }


    @Transactional
    @Override
    public boolean deleteAllById(List<Long> listId){
        log.info("--Delete a news by id--");

        List<News> listNews = new ArrayList<>();
        listId.forEach(id -> {
                Optional<News> news =
             Optional.of(newsRepository.findById(id))
                .filter(Optional::isPresent)
                .map(Optional::get);
             if (!news.isPresent()) throw new BadRequestAlertException(Alerts.ID_EXIST, "product-manager", "id");
             listNews.add(news.get());
        });
        newsRepository.deleteAll(listNews);
        return true;
    }
    @Override
    public HashMap<String,?> getAllManagedNews(int page, int size) {
        log.info("--Get all user table --");
        return ArrayDTO.convertListToSet(newsRepository.count(),
            newsRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))).getContent());
    }



    @Override
    public HashMap<String, ?> searchNews(String param, int page, int size) {
        log.info("--Search --");
        return
                ArrayDTO.convertListToSet(
                        newsRepository.countSearch(param),
                        newsRepository.searchNewsByAll(param,PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")))
                );
    }
}
