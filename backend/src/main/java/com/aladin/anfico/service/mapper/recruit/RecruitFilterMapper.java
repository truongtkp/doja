package com.aladin.anfico.service.mapper.recruit;

import com.aladin.anfico.domain.Recruit;
import com.aladin.anfico.service.dto.recruit.RecruitFillterDTO;
import com.aladin.anfico.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Recruit} and its DTO {@link RecruitFillterDTO}.
 */
@Mapper(componentModel = "spring")
public interface RecruitFilterMapper extends EntityMapper<RecruitFillterDTO, Recruit> {

}
