package com.aladin.anfico.service.dto.news;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewsCreateDTO {

    private Long id;

    @NotNull
    @Size(min = 0, max = 500)
    private String titleVi;

    @NotNull
    @Size(min = 0, max = 500)
    private String titleEn;

    @NotNull
    private String descriptionVi;

    @NotNull
    private String descriptionEn;

    @NotNull
    private String contentVi;

    @NotNull
    private String contentEn;

    @NotNull
    @Size(min = 0, max = 500)
    private String avatarUrl;

    @NotNull
    @Size(min = 0, max = 500)
    private String avatarPath;

    private Instant createdDate;
}
