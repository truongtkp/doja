package com.aladin.anfico.service.dto.recruit;


import com.aladin.anfico.config.Constants;
import com.aladin.anfico.domain.Candidate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CandidateDTO {

    private Long id;

    @NotNull
    @Size(min = 5, max = 256)
    private String fullname;

    @NotNull
    private boolean gender;

    @Email
    @NotNull
    @Size(min = 5, max = 254)
    private String email;

    @NotNull
    @Size(min = 5, max = 256)
    private String address;

    @NotNull
    @Size(min = 5, max = 50)
    private String education;

    @NotNull
    private String experience;

    @Pattern(regexp = Constants.PHONE_REGEX)
    @NotNull
    private String phone;

    @NotNull
    private Date birthday;

    @Size(min = 5, max = 50)
    private String goal;

    private String presenter;

    @NotNull
    @Size(min = 5, max = 256)
    private String cvUrl;

    @NotNull
    @Size(min = 5, max = 256)
    private String cvPath;

}
