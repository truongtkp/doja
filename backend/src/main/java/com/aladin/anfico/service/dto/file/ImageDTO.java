
package com.aladin.anfico.service.dto.file;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ImageDTO {

    public ImageDTO(String image_path, String image_url) {
        this.image_path = image_path;
        this.image_url = image_url;
    }
    String image_path;
    String image_url;

}
