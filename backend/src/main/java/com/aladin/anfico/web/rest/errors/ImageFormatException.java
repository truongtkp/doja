package com.aladin.anfico.web.rest.errors;

public class ImageFormatException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public ImageFormatException(String text) {
        super(ErrorConstants.IMAGE_FORMAT_READY, text, "image-manager", "image-error");
    }
}
