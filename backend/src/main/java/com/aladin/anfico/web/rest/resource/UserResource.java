package com.aladin.anfico.web.rest.resource;

import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.UserService;
import com.aladin.anfico.service.dto.array.ArrayDTO;
import com.aladin.anfico.web.rest.errors.PageableRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;

@RestController
@RequestMapping("/api/admin")
@RequiredArgsConstructor
@Tag(name = "user-management",description = "CRUD - OK")

public class UserResource {
    private final UserService userServiceImpl;

    private final Logger log = LoggerFactory.getLogger(com.aladin.anfico.web.rest.resource.UserResource.class);

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(@RequestParam int page, @RequestParam int limit ) {
        log.info("[Anfico App]: GET /api/admin/users -- Get all information  user table");
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        HashMap<String,?> pages =  userServiceImpl.getAllManagedUsers(page,limit);
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }

    @PutMapping("/user/active/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Boolean> activeUser(@PathVariable String id) {
        log.info("[Anfico App]: DELETE /api/admin/user/active -- active information give user table--");
        return new ResponseEntity(userServiceImpl.activeUser(id), null, HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Boolean> deleteUser(@PathVariable String id) {
        log.info("[Anfico App]: DELETE /api/admin/user/delete -- delete information give user table--");
        if (userServiceImpl.deleteUser(id))
            return new ResponseEntity(true, null, HttpStatus.OK);
        return new ResponseEntity(false, null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/user/search/{param}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<HashMap<String, ?>> searchUser(@PathVariable String param, @RequestParam int page, @RequestParam int limit){
        log.info("[Anfico App]: GET /api/admin/search -- delete {} information give user table--", param);
        if (!ArrayDTO.checkPageable(page,limit)) throw new PageableRequestAlertException();
        return new ResponseEntity<>(userServiceImpl.searchUser(param, page, limit), HttpStatus.OK);
    }
}

