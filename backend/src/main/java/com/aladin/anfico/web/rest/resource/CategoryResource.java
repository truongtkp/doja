package com.aladin.anfico.web.rest.resource;


import com.aladin.anfico.config.Alerts;
import com.aladin.anfico.domain.Category;
import com.aladin.anfico.security.AuthoritiesConstants;
import com.aladin.anfico.service.CategoryService;
import com.aladin.anfico.service.dto.category.CategoryDTO;
import com.aladin.anfico.web.rest.errors.BadRequestAlertException;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Tag(name = "category",description = "")
public class CategoryResource {
    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    private final CategoryService categoryServiceImpl;

    @GetMapping("/categories")
    public ResponseEntity<HashMap<String,?>> getAllCategory() {
        log.info("[Anfico App]: GET /api/categories -- Get all category in category table");
        HashMap<String,?> pages =  categoryServiceImpl.getAllManagedCategory();
        return new ResponseEntity<>(pages, HttpStatus.OK);
    }

    @PostMapping("/category")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Category> addCategory(@Valid @RequestBody CategoryDTO categoryDTO ) {
        log.info("[Anfico App]: POST /api/categories -- add list category in category table");
        if (categoryDTO.getId() != null)
            throw new BadRequestAlertException(Alerts.ID_NULL, "category-management", "id");

        Category category =  categoryServiceImpl.addCategory(categoryDTO);
        return  ResponseEntity.ok().body(category);
    }

    @PutMapping("/category")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Category> updateCategory(@Valid @RequestBody CategoryDTO categoryDTO ){
        log.info("[Anfico App]: PUT /api/category -- update a in news table");
        if (categoryDTO.getId() == null)
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "category-management", "id");
        return ResponseUtil.wrapOrNotFound(categoryServiceImpl.updateCategory(categoryDTO));
    }

    @DeleteMapping("/category/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Boolean> deleteCategory(@PathVariable Long id){
        log.info("[Anfico App]: PUT /category/status/{id} -- active news in category table");
        Optional<Category> category  =  categoryServiceImpl.deleteCategory(id);
        if (!category.isPresent()){
            throw new BadRequestAlertException(Alerts.ID_NOT_NULL, "category-management", "id");
        }
        return ResponseEntity.ok().body(true);
    }
}
