package com.aladin.anfico.web.rest.errors;

import java.net.URI;

public class PhoneFormatException extends BadRequestAlertException{

    private static final long serialVersionUID = 1L;

    public PhoneFormatException() {
        super(ErrorConstants.FORMAT_PHONE_FAIL, "Contact not format", "contact", "format");
    }


}
