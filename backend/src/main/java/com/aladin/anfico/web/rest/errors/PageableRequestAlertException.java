package com.aladin.anfico.web.rest.errors;



public class PageableRequestAlertException extends BadRequestAlertException{

    private static final long serialVersionUID = 1L;

    public PageableRequestAlertException() {
        super("Size and page must be greater than or equal to 0", "pageable", "format");
    }
}
