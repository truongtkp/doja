package com.aladin.anfico.config;

public final class Alerts {
    public static final String ID_NULL = "Id must null!";
    public static final String ID_NOT_NULL = "Id not null!";
    public static final String ID_EXIST  = "Id doesn't exist!";
    public static final String PRIORITY  = "Project more than 4 !";
    public static final String YEAR_EXIST = "Year exists!";
}
