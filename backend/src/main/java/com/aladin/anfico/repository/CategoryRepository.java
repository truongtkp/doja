package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Category;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepository  extends JpaRepository<Category, Long> {

    @Query("select c.id from Category c inner join c.products p where  p.id = ?1")
    Long getCategoryByProductID(Long id);
}
