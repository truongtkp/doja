package com.aladin.anfico.repository;

import com.aladin.anfico.domain.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HistoryRepository extends JpaRepository<History, Long> {
    Optional<History> findOneByYear(String year);
}
