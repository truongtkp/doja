package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    @Query("select c from Candidate c where c.recruit.id = ?1 order by c.id desc ")
    List<Candidate> getCandidateByRecruitId(Long id, PageRequest pageRequest);
    @Query("select count(c.id) from Candidate c where c.recruit.id = ?1 ")
    int getCountByRecruit(Long id);

    @Query(
        value = "select c from Candidate c where c.recruit.id = :recruitId order by c.id desc ",
        countQuery = "select count(c.id) from Candidate c where c.recruit.id = :recruitId order by c.id desc"
    )
    Page<Candidate> findAllByRecruitId(@Param("recruitId") Long recruitId, Pageable page);

}
