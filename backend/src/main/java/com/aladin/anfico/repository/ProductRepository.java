package com.aladin.anfico.repository;

import com.aladin.anfico.domain.Product;
import com.aladin.anfico.service.dto.product.ProductDTO;
import com.aladin.anfico.service.dto.search.SearchDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {


    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl, p.priority) from " +
            "Product p join p.productImages pi group by p.id ")
    List<ProductDTO> getAllProduct(PageRequest pageRequest);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.id = ?1 group by p.id order by p.id desc")
    ProductDTO getProductById(Long id);


    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.titleVi like %?1%  group by p.id")
    List<ProductDTO> searchProductVi(String param, PageRequest pageRequest);

    @Query("select count(p.id) from Product p  where p.titleVi like %?1%")
    Long countSearchProductVi(String param);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl, p.priority) from " +
            "Product p join p.productImages pi where  p.titleEn like %?1% group by p.id")
    List<ProductDTO> searchProductEn(String param, PageRequest pageRequest);

    @Query("select count(p.id) from Product p where  p.titleEn like %?1% ")
    Long countSearchProductEn(String param);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.category.id = ?1 group by p.id")
    List<ProductDTO> getProductByCategory(Long categoryId, PageRequest pageRequest);

    @Query("select count(p.id) from Product p  where p.category.id = ?1")
    Long countProductByCategory(Long categoryId);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.category.id = ?2 and  p.titleVi like %?1%  group by p.id")
    List<ProductDTO> searchProductByCategoryVi(String param, Long categoryId, PageRequest pageRequest);

    @Query("select count(p.id) from Product p  where p.category.id = ?2 and  p.titleVi like %?1% ")
    Long countSearchProductByCategoryVi(String param, Long categoryId);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.category.id = ?2 and p.titleEn like %?1%  group by p.id")
    List<ProductDTO> searchProductByCategoryEn(String param, Long categoryId, PageRequest pageRequest);

    @Query("select count(p.id) from Product p  where p.category.id = ?2 and  p.titleEn like %?1%")
    Long countSearchProductByCategoryEn(String param, Long categoryId);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where  p.titleEn like %?1% group by p.id")
    List<SearchDTO> searchProductEn(String param);

    @Query("select new Product(p.id,p.titleEn,p.titleVi,p.descriptionVi,p.descriptionEn, pi.imageUrl,  p.priority) from " +
            "Product p join p.productImages pi where p.titleVi like %?1%  group by p.id")
    List<SearchDTO> searchProductVi(String param);

    @Query("select count(c.id) from Product c where c.priority = true ")
    int countProjectByPriority();
}
