package com.aladin.anfico.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@Table(name = "supplier_info")
@AllArgsConstructor
@NoArgsConstructor
public class SupplierInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 70)
    @Size(max = 70)
    private String fullname;

    @Column(length = 500)
    @Size(max = 500)
    private String link;

    @Column(name = "avatar_url", length = 500)
    @Size(max = 500)
    private String avatarUrl;

    @Column(name = "avatar_path", length = 500)
    @Size(max = 500)
    private String avatarPath;

    private Social social;

}
