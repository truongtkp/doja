package com.aladin.anfico.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "specification")
@Getter
@Setter
public class Specification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_en", length = 256)
    @Size(max = 256)
    private String nameEn;

    @Column(name = "name_vi", length = 256)
    @Size(max = 256)
    private String nameVi;

    @JsonIgnore
    @ManyToOne
    private Product _product;

    @OneToMany(mappedBy = "specification", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<SpecificationDetail> specificationDetails = new HashSet<>();
}
